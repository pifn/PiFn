#!/bin/bash

TEST_NAME="$(echo "$0" | sed -e 's_.*/__' -e 's_\..*__')"
echo "RUNNING TEST: $TEST_NAME"

function show_file
{
	echo "----- BEGIN: $1 -----"
	cat "$1"
	echo "----- END: $1 -----"
}

function pifnd_stop
{
	echo "stopping pifnd, pid $PIFND_PID"
	if kill "$PIFND_PID"
	then
		wait "$PIFND_PID"
		echo "pifnd stopped"
		PIFND_PID=
		PIFND_PORT=
	else
		echo "could not stop pifnd"
		exit 1
	fi
}

function check_exit
{
	rt=$?
	if [ $rt -eq 0 ]
	then
		echo "PASS: $1"
	else
		echo "FAIL: $1"
		echo "exit code $rt, $2"
		[ -n "$PIFND_PID" ] && pifnd_stop
		exit $rt
	fi
}

function pifnd_start
{
	logfile="$TEST_NAME.pifnd.log"
	echo "starting pifnd, output logged in $logfile"

	../pifnd/pifnd -c "$srcdir/pifnd.conf" &> "$logfile"&
	PIFND_PID=$!

	PIFND_PORT=
	for i in $(seq 1 10)
	do
		sleep 1
		PIFND_PORT="$(head -n1 "$logfile" | sed 's/.*://')"
		kill -0 $PIFND_PID
		if [ $? -ne 0 ]
		then
			echo "pifnd died unexpectedly"
			show_file "$logfile"
			exit 1
		fi

		if [[ "$PIFND_PORT" =~ [0-9]+ ]]
		then
			echo "pifnd started on port $PIFND_PORT, pid $PIFND_PID"
			return
		fi
	done

	echo "pifnd hasn't bound to a port"
	show_file "$logfile"
	pifnd_stop
	exit 1
}

function pifn_cmd
{
	TIMEOUT="$1"
	shift
	(
		ulimit -c unlimited &&
		exec timeout -s SIGABRT -k 10 "$TIMEOUT" ../pifn/pifn --port $PIFND_PORT "$@" ../pifn/pifn.conf
	)
}

function check_equal
{
	[ "$1" = "$2" ]
	check_exit "$3" "expecting '$1', got '$2'"
}
