#!/bin/bash

. $srcdir/util.sh

pifnd_start

RET="$(pifn_cmd 10 -t '(echo "echo Hello World"; sleep 1)')"
check_exit "shell channel"
check_equal "Hello World" "$RET" "echo result"

pifnd_stop
