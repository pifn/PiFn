#!/bin/bash

. $srcdir/util.sh

pifnd_start

# Repeat test 30 times as it can fail due to a race
for i in $(seq 1 30)
do
	RET="$(pifn_cmd 10 -t 'echo "echo Hello World"')"
	check_exit "shell channel quick close"
	check_equal "Hello World" "$RET" "echo result, quick close"
done

pifnd_stop
