#include <packet.h>

#include <arpa/inet.h>
#include <boost/asio.hpp>
#include <check.h>
#include <iostream>
#include <stdlib.h>
#include <sstream>

using namespace pifn;
using namespace std;

START_TEST(header_parse)
{
	uint16_t memory[2];
	memory[0] = htons(0x0102);
	memory[1] = htons(0x0304);
	stringstream ins(string((char*)memory, sizeof(memory)));

	packet_header header(ins);
	ck_assert(header.channel() == 0x0102);
	ck_assert(header.length() == 0x0304);
}
END_TEST

START_TEST(header_render)
{
	boost::asio::streambuf buffer;

	packet_header header(0x0102, 0x0304);
	ostream outs(&buffer);
	header.render(outs);

	ck_assert(buffer.size() == 2 * sizeof(uint16_t));

	uint16_t i;
	istream ins(&buffer);

	ins.read((char*)&i, sizeof(uint16_t));
	ck_assert(i == htons(0x0102));

	ins.read((char*)&i, sizeof(uint16_t));
	ck_assert(i == htons(0x0304));
}
END_TEST

Suite * packet_suite(void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create("Packet Header");

	/* Core test case */
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, header_parse);
	tcase_add_test(tc_core, header_render);
	suite_add_tcase(s, tc_core);

	return s;
}

int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = packet_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
