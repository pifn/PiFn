#!/bin/bash

. $srcdir/util.sh

pifnd_start

CLEAN_FILES="data_pipe.in data_pipe.out data_pipe.count"
rm -f $CLEAN_FILES
mkfifo data_pipe.in
mkfifo data_pipe.out

dd if=/dev/zero bs=1k count=20k of=data_pipe.in &
STOP_LATER=$!

wc -c < data_pipe.out > data_pipe.count &
STOP_LATER="$STOP_LATER $!"

START_TIME=`date +%s`
pifn_cmd 30 --channel echo < data_pipe.in > data_pipe.out
RT=$?
END_TIME=`date +%s`
BYTES="$(cat data_pipe.count)"
echo "$BYTES transfered in $(($END_TIME - $START_TIME)) second(s)"

(exit $RT)
check_exit "echo channel"
check_equal "20971520" "$BYTES" "echo channel bytes returned"

for pid in $STOP_LATER
do
	kill $pid
done

rm -f $CLEAN_FILES

pifnd_stop
