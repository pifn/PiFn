#ifndef _INSPECT_H

/*
 * Packet inspection
 *
 * If PACKET_INSPECT or PACKET_DUMP is defined then the function
 * packet_inspect(bool, uint16_t, const boost::asio::streambuf&) is defined,
 * otherwise a macro of the same name is define that compiles away to
 * nothing.
 *
 * If PACKET_INSPECT is defined this function will dump the packet to
 * std::cerr as both hex bytes and printable ascii.
 *
 * If PACKET_DUMP is defined the packet is written in binary form to a
 * packets.out file in the CWD. This file is opened when the first packet to
 * be written to them is inspected. Each packet written is preceeded by a
 * single byte, the lest significant bit set to 0 if sending and 1 if
 * receiving.
 *
 * If both PACKET_INSPECT and PACKET_DUMP is defined then the packet is
 * dumped to both cerr and the file.
 */

#define INSFLAG_NONE 0x00
#define INSFLAG_SEND 0x01

#if defined PACKET_INSPECT || defined PACKET_DUMP

#include <boost/asio.hpp>

void packet_inspect(uint32_t connection, uint8_t flags, uint16_t channel, boost::asio::const_buffer buffer);
void packet_inspect(uint32_t connection, uint8_t flags, const std::vector<boost::asio::const_buffer>& buffers);

#else

#define packet_inspect(...) ((void)0)

#endif

#endif

