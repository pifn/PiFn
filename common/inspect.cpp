#include "inspect.h"

#if defined PACKET_INSPECT || defined PACKET_DUMP

#include "hexdump.h"

#include <netinet/in.h>
#include <boost/asio/buffer.hpp>
#include <boost/thread.hpp>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>

using namespace std;
using boost::asio::buffer_cast;
using boost::asio::buffer_size;
using boost::asio::const_buffer;

#ifdef PACKET_INSPECT
static void packet_render(uint32_t /*connection*/, uint8_t flags, uint16_t channel, const_buffer& buffer)
{
	const size_t size = buffer_size(buffer);
	cerr << ((flags & INSFLAG_SEND) ? "s" : "r") << setw(3) << setfill(' ') << channel << ":";

	const char* data = buffer_cast<const char*>(buffer);
	for(size_t i = 0; i < size; i += hex_block)
	{
		if(i > 0)
			cerr << "     ";
		size_t end = i + hex_block;
		if(end > size)
			end = size;
		hexdump(cerr, i, &data[i], end - i);
		cerr << endl;
	}
	if(size == 0)
		cerr << endl;
}
#endif

#ifdef PACKET_DUMP
static unique_ptr<ostream> dump_stream;
static boost::mutex dump_lock;

static void packet_dump(uint32_t connection, uint8_t flags, uint16_t channel, const const_buffer& buffer)
{
	boost::lock_guard<boost::mutex> guard(dump_lock);
	if(!dump_stream)
		dump_stream.reset(new ofstream("packets.out"));
	if(*dump_stream)
	{
		dump_stream->write(reinterpret_cast<const char*>(&flags), sizeof(flags));

		connection = htonl(connection);
		dump_stream->write(reinterpret_cast<const char*>(&connection), sizeof(connection));

		channel = htons(channel);
		dump_stream->write(reinterpret_cast<const char*>(&channel), sizeof(channel));

		assert(buffer_size(buffer) <= 0xFFFF);
		uint16_t size = htons(buffer_size(buffer));
		dump_stream->write(reinterpret_cast<const char*>(&size), sizeof(size));

		dump_stream->write(buffer_cast<const char*>(buffer), buffer_size(buffer));
	}
}
#endif

void packet_inspect(uint32_t connection, uint8_t flags, uint16_t channel, const_buffer buffer)
{
#ifdef PACKET_INSPECT
	packet_render(connection, flags, channel, buffer);
#endif
#ifdef PACKET_DUMP
	packet_dump(connection, flags, channel, buffer);
#endif
}

void packet_inspect(uint32_t connection, uint8_t flags, const std::vector<const_buffer>& buffers)
{
	const uint16_t* header = buffer_cast<const uint16_t*>(buffers[0]);
	uint16_t channel = ntohs(header[0]);
	packet_inspect(connection, flags, channel, static_cast<const_buffer>(buffers[1]));
}

#endif
