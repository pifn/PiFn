#ifndef _ACTION_H
#define _ACTION_H

#include "pqueue.h"

#include <functional>

namespace pifn {
	class pauseable_action : public pproducer
	{
	private:
		int _pause_count;
		bool _triggered;

	public:
		pauseable_action();

		void trigger();
		virtual void pause();
		virtual void resume();
		virtual void queue_closed();

	protected:
		virtual void action() = 0;
	};
}

#endif
