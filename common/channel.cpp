#include "channel.h"
#include "util.h"

#include <algorithm>
#include <cstring>

using namespace pifn;
using namespace std;

const size_t channel_action::TYPE_SIZE = 7;


channel_action::channel_action() :
action(0),
target(0),
type(string()),
type_data(string())
{
}

channel_action::channel_action(channel_action&& that) :
action(that.action),
target(that.target),
type(move(that.type)),
type_data(move(that.type_data))
{
}

channel_action::channel_action(const channel_action& that) :
action(that.action),
target(that.target),
type(that.type),
type_data(that.type_data)
{
}

channel_action::channel_action(uint8_t action0, uint16_t target0, string type0, string type_data0) :
action(action0),
target(target0),
type(type0),
type_data(type_data0)
{
	if(type.length() > TYPE_SIZE)
		throw out_of_range(MKSTR("type name too long: " << type));
}

channel_action::channel_action(std::istream& in, size_t size)
{
	parse(in, size);
}

channel_action& channel_action::operator=(const channel_action& that)
{
	action = that.action;
	target = that.target;
	type = that.type;
	type_data = that.type_data;
	return *this;
}

channel_action& channel_action::operator=(channel_action&& that)
{
	action = that.action;
	target = that.target;
	type = move(that.type);
	type_data = move(that.type_data);
	return *this;
}

void channel_action::parse(std::istream& in, size_t size)
{
	deserialiser de(in);

	target = de.read_uint16();
	action = de.read_uint8();
	type = de.read_string(min(size - MIN_SIZE, TYPE_SIZE));

	if(de.count() < size && size > MIN_SIZE + TYPE_SIZE)
		type_data = de.read_string(size - de.count());
	else
		type_data = string();
}

size_t channel_action::render(std::ostream& out) const
{
	serialiser sr(out);

	sr.write_uint16(target);
	sr.write_uint8(action);
	sr.write_string(type, type_data.empty() ? type.length() : TYPE_SIZE);
	sr.write_string(type_data);

	return sr.count();
}
