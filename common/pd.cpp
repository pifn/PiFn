#include <hexdump.h>
#include <inspect.h>
#include <packet.h>

#include <boost/crc.hpp>
#include <boost/program_options.hpp>
#include <map>
#include <list>
#include <netinet/in.h>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <stdint.h>
#include <string>

namespace po = boost::program_options;
using namespace pifn;
using namespace std;
using boost::asio::buffer_cast;

#define OPT_HEXLIM "hex-limit"
#define DEC_HEXLIM OPT_HEXLIM ",h"

#define OPT_LAST "last"
#define DEC_LAST OPT_LAST ",l"

#define OPT_FILE "file"
#define DEC_FILE OPT_FILE

#define OPT_NUMBER "number"
#define DEC_NUMBER OPT_NUMBER ",n"

#define OPT_CHANNEL "channel"
#define DEC_CHANNEL OPT_CHANNEL ",c"

#define OPT_DIR "direction"
#define DEC_DIR OPT_DIR ",d"

#define OPT_CONNECTION "connection"
#define DEC_CONNECTION OPT_CONNECTION ",e"

#define OPT_FORMAT "format"
#define DEC_FORMAT OPT_FORMAT ",f"

po::options_description op_desc("options");
po::options_description op_hidden;
po::options_description op_both;
po::positional_options_description op_pos;
po::variables_map op_vm;

class pair_count
{
public:
	int sent;
	int recv;

	pair_count() : sent(0), recv(0) {}
};

//Keeps track of how many packets have been sent or recieved so far
class file_context
{
public:
	file_context() : total(0) {}

	size_t total;
	map<uint16_t, pair_count> channels;
};

//A single packet
class record
{
public:
	uint8_t flags;
	uint16_t connection;
	size_t total_offset, stream_offset, size;
	packet p;

	record() : p(0) {}

	//parse a packet, return true if successful
	bool parse(istream& in, file_context& context)
	{
		p.payload.consume(p.payload.size());

		uint8_t p_flags;
		uint32_t p_connection;
		uint16_t p_channel, p_size;
		in.read(reinterpret_cast<char*>(&p_flags), sizeof(p_flags));
		in.read(reinterpret_cast<char*>(&p_connection), sizeof(p_connection));
		in.read(reinterpret_cast<char*>(&p_channel), sizeof(p_channel));
		in.read(reinterpret_cast<char*>(&p_size), sizeof(p_size));
		if(!in)
			return false;

		flags = p_flags;
		connection = ntohl(p_connection);
		p.channel = ntohs(p_channel);
		size = ntohs(p_size);
		total_offset = context.total++;

		pair_count& pair = context.channels[p.channel];
		int& ch_count = flags & INSFLAG_SEND ? pair.sent : pair.recv;
		stream_offset = ch_count++;

		in.read(buffer_cast<char*>(p.payload.prepare(size)), size);
		p.payload.commit(in.gcount());

		return true;
	}

	size_t missing() const
	{
		return size - p.payload.size();
	}
};

class filter
{
public:
	virtual bool accept(const record&) const = 0;
};

class filter_chain : public filter
{
private:
	vector<unique_ptr<filter>> _filters;

public:
	filter_chain()
	{
	}

	template<typename Filter, typename... Params>
	void add(Params... args)
	{
		unique_ptr<Filter> f(new Filter(forward<Params>(args)...));
		_filters.emplace<unique_ptr<Filter>>(_filters.end(), move(f));
	}

	bool accept(const record& r) const
	{
		for(auto filter = _filters.begin(); filter != _filters.end(); ++filter)
			if(!(*filter)->accept(r))
				return false;
		return true;
	}
};

class number_filter : public filter
{
private:
	unsigned long int _offset;
	bool _before;

public:
	number_filter(unsigned long int offset, bool before) :
	_offset(offset),
	_before(before)
	{}

	bool accept(const record& r) const
	{
		if(_before)
			return r.total_offset <= _offset;
		else
			return r.total_offset >= _offset;
	}
};

class channel_filter : public filter
{
private:
	uint16_t _channel;

public:
	channel_filter(uint16_t channel) : _channel(channel) {}

	bool accept(const record& r) const
	{
		return r.p.channel == _channel;
	}
};

class direction_filter : public filter
{
private:
	uint8_t _match;

public:
	direction_filter(bool sent) : _match(sent ? INSFLAG_SEND : INSFLAG_NONE) {}

	bool accept(const record& r) const
	{
		return (r.flags & INSFLAG_SEND) == _match;
	}
};

class connection_fileter : public filter
{
private:
	uint32_t _connection;

public:
	connection_fileter(uint32_t connection) : _connection(connection) {}

	bool accept(const record& r) const
	{
		return r.connection == _connection;
	}
};

class printer
{
public:
	virtual void print(const record& r) const = 0;
};

class ascii_printer : public printer
{
public:
	void print(const record& r) const
	{
		boost::crc_16_type crc;
		crc.process_bytes(
			buffer_cast<const char*>(r.p.payload.data()),
			r.p.payload.size()
		);

		cout << "#" << r.total_offset
			<< " con=" << r.connection
			<< " ch=0x" << hex << r.p.channel << dec
			<< " dir=" << ((r.flags & INSFLAG_SEND) ? "send" : "recv")
			<< " dirnum=" << r.stream_offset
			<< " length=" << r.size
			<< " crc=0x" << setfill('0') << setw(4) << hex << crc.checksum() << dec
			<< endl;

		size_t limit = r.p.payload.size();
		if(op_vm.count(OPT_HEXLIM))
			limit = op_vm[OPT_HEXLIM].as<size_t>();
		const char* buffer = buffer_cast<const char*>(r.p.payload.data());
		size_t end = min(r.p.payload.size(), limit);
		for(size_t i = 0; i < end; i += hex_block)
		{
			size_t n = min(hex_block, end - i);
			hexdump(cout, i, &buffer[i], n);
			cout << endl;
		}

		if(r.missing())
			cout << r.missing() << " bytes missing" << endl;
	}
};

class binary_printer : public printer
{
private:
	const bool _direction, _header;

public:
	binary_printer(bool direction, bool header) :
	_direction(direction),
	_header(header)
	{
	}

	void print(const record& r) const
	{
		if(_direction)
		{
			cout.write(reinterpret_cast<const char*>(&r.flags), sizeof(r.flags));
			uint32_t connection = htonl(r.connection);
			cout.write(reinterpret_cast<const char*>(&connection), sizeof(connection));
		}

		if(_header)
		{
			uint16_t i = htons(r.p.channel);
			cout.write(reinterpret_cast<const char*>(&i), sizeof(i));
			i = htons(r.size);
			cout.write(reinterpret_cast<const char*>(&i), sizeof(i));
		}

		cout.write(buffer_cast<const char*>(r.p.payload.data()), r.p.payload.size());
	}
};

void dump_file(istream& in, const filter& filters, const printer& printer)
{
	file_context ctx;
	record r;
	while(in && cout)
		if(r.parse(in, ctx) && filters.accept(r))
			printer.print(r);
}

void read_last(istream& in, const filter& filters, const printer& printer)
{
	file_context ctx;
	const size_t n = op_vm[OPT_LAST].as<size_t>();
	if(n <= 0)
		return;
	list<unique_ptr<record>> records;

	unique_ptr<record> next(new record);
	while(in && next->parse(in, ctx))
	{
		if(filters.accept(*next))
		{
			records.emplace<unique_ptr<record>>(
				records.end(),
				unique_ptr<record>(next.release())
			);
			next.reset(new record);
		}

		if(records.size() > n)
			records.pop_front();
	}

	for(auto it = records.begin(); it != records.end(); ++it)
		printer.print(**it);
}

void process_stream(istream& in, const filter& filters, const printer& printer)
{
	if(op_vm.count(OPT_LAST))
		read_last(in, filters, printer);
	else
		dump_file(in, filters, printer);
}

int process_file(const string& file, const filter& filters, const printer& printer)
{
	if(file == "-")
	{
		process_stream(cin, filters, printer);
		return 0;
	}

	ifstream in(file);
	if(!in)
	{
		cout << "Failed to open file: " << file << endl;
		return 1;
	}

	process_stream(in, filters, printer);

	in.close();
	return 0;
}

void usage(const char* me)
{
	cerr << "usage: " << me << " [options] <dump_file>" << endl
		<< op_desc
		<< "to select a single packet (e.g. packet #7) use -n7" << endl
		<< "to select a range of packets (e.g. packets #7 to #10) use -n7-10" << endl
		<< "output formats:" << endl
		<< "  a\thuman readable ascii (default)" << endl
		<< "  r\traw, packet payload is written as binary" << endl
		<< "  h\theaders, as raw but proceeded by packet headers" << endl
		<< "  d\tdump format, same as is read by this program" << endl
		<< endl;
}

int main(int argc, char** argv)
{
	unique_ptr<printer> printer;
	filter_chain fc;
	try
	{
		op_desc.add_options()
			(DEC_FORMAT, po::value<char>(), "output format (see below)")
			(DEC_HEXLIM, po::value<size_t>(), "limit hex dump length, 0 for none")
			(DEC_CHANNEL, po::value<uint16_t>(), "filter dump for channel")
			(DEC_DIR, po::value<char>(), "filter dump for direction ('s' or 'r' for sent or received)")
			(DEC_NUMBER, po::value<string>(), "filter out all but a packet or a range of packets")
			(DEC_CONNECTION, po::value<uint32_t>(), "filter dump for connection")
			(DEC_LAST, po::value<size_t>(), "display only the last n packets")
		;
		op_hidden.add_options()
			(DEC_FILE, po::value<string>(), "Packet file")
		;
		op_both.add(op_desc).add(op_hidden);
		op_pos.add(DEC_FILE, -1);
		po::store(po::command_line_parser(argc, argv).options(op_both).positional(op_pos).run(), op_vm);
		po::notify(op_vm);

		if(!op_vm.count(OPT_FILE))
			throw invalid_argument("No file to read");

		if(op_vm.count(OPT_NUMBER))
		{
			string first = op_vm[OPT_NUMBER].as<string>();
			string last;
			auto hyphen = first.find('-');
			if(hyphen == string::npos)
				last = first;
			else
			{
				last = first.substr(hyphen + 1);
				first = first.substr(0, hyphen);
			}
			fc.add<number_filter>(stoul(first), false);
			if(!last.empty())
				fc.add<number_filter>(stoul(last), true);
		}

		char format = 'a';
		if(op_vm.count(OPT_FORMAT))
			format = op_vm[OPT_FORMAT].as<char>();
		switch(format)
		{
		case 'a':
			printer.reset(new ascii_printer);
			break;
		case 'd':
			printer.reset(new binary_printer(true, true));
			break;
		case 'h':
			printer.reset(new binary_printer(false, true));
			break;
		case 'r':
			printer.reset(new binary_printer(false, false));
			break;
		default:
			throw invalid_argument("unknown format");
		}

		if(op_vm.count(OPT_CHANNEL))
			fc.add<channel_filter>(op_vm[OPT_CHANNEL].as<uint16_t>());

		if(op_vm.count(OPT_CONNECTION))
			fc.add<connection_fileter>(op_vm[OPT_CONNECTION].as<uint32_t>());

		if(op_vm.count(OPT_DIR))
		{
			switch(op_vm[OPT_DIR].as<char>())
			{
			case 's':
				fc.add<direction_filter>(true);
				break;
			case 'r':
				fc.add<direction_filter>(false);
				break;
			default:
				throw invalid_argument("Argument to --" OPT_DIR " should be either 's' or 'r'");
			}
		}
	}
	catch(exception& e)
	{
		cerr << e.what() << endl;
		usage(argv[0]);
		return 1;
	}

	if(process_file(op_vm[OPT_FILE].as<string>().c_str(), fc, *printer))
		return 1;
	return 0;
}
