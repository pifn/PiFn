#include "hexdump.h"

#include <iomanip>

using namespace std;

size_t hex_block = 16;
size_t hex_group = 4;

void hexdump(ostream& out, size_t offset, const char* data, size_t length)
{
	out << uppercase << hex << setfill('0') << setw(4) << offset << " ";
	for(size_t i = 0; i < hex_block; ++i)
	{
		if(i != 0 && (i % hex_group) == 0)
			out << "  ";
		if(i < length)
			out << " " << setw(2) << static_cast<int>(static_cast<unsigned char>(data[i]));
		else
			out << "   ";
	}

	out << nouppercase << dec << setw(0)<< "  ";
	for(size_t i = 0; i < length; ++i)
	{
		if(i != 0 && (i % hex_group) == 0)
			out << " ";
		if(data[i] >= ' ' && data[i] < 127)
			out << data[i];
		else
			out << '.';
	}
}
