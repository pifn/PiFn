#include "action.h"

using namespace pifn;

pauseable_action::pauseable_action() :
_pause_count(0),
_triggered(false)
{
}

void pauseable_action::trigger()
{
	assert(!_triggered);
	if(_pause_count)
		_triggered = true;
	else
		action();
}

void pauseable_action::pause()
{
	++_pause_count;
}

void pauseable_action::resume()
{
	assert(_pause_count > 0);
	--_pause_count;
	if(!_pause_count && _triggered)
	{
		_triggered = false;
		action();
	}
}

void pauseable_action::queue_closed()
{
}
