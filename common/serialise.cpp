#include "serialise.h"

#include <netinet/in.h>
#include <sstream>

using namespace pifn;
using boost::asio::streambuf;

serialiser::serialiser(std::ostream& out) :
_out(out),
_count(0)
{
}

size_t serialiser::count() const
{
	return _count;
}

size_t serialiser::write(const serialisable& sr)
{
	size_t size = sr.render(_out);
	_count += size;
	return size;
}

void serialiser::write(const void* data, const size_t size)
{
	_out.write((const char*)data, size);
	_count += size;
}

void serialiser::write_uint8(uint8_t data)
{
	_out.put((char)data);
	++_count;
}

void serialiser::write_uint16(uint16_t data)
{
	const uint16_t i = htons(data);
	write(&i, sizeof(i));
}

size_t serialiser::write_string(const std::string& data, const size_t pad_to)
{
	write(data.data(), data.size());
	size_t written = data.size();
	for(; written < pad_to; written++)
	{
		_out.put('\0');
		++_count;
	}
	return written;
}

deserialiser::deserialiser(std::istream& in) :
_in(in),
_count(0)
{
}

size_t deserialiser::count() const
{
	return _count;
}

void deserialiser::read(void* data, const size_t size)
{
	_in.read((char*)data, size);
	size_t r = _in.gcount();
	_count += r;
	if(r < size)
		throw std::out_of_range("stream too short");
}

void deserialiser::read(deserialisable& de, const size_t size)
{
	de.parse(_in, size);
	_count += size;
}

uint8_t deserialiser::read_uint8()
{
	uint8_t i;
	read(&i, sizeof(i));
	return i;
}

uint16_t deserialiser::read_uint16()
{
	uint16_t i;
	read(&i, sizeof(i));
	return ntohs(i);
}

std::string deserialiser::read_string(size_t size, bool trim_padding)
{
	if(size == 0)
		return "";

	char c;
	bool end = false;
	std::stringstream ss;

	for(size_t read = 0; read < size; read++)
	{
		++_count;
		c = _in.get();
		if(end)
			continue;
		if(c == '\0' && trim_padding)
			end = true;
		else
			ss << c;
	}

	return ss.str();
}

size_t pifn::streambuf_parse(deserialisable& de, streambuf& buf)
{
	std::istream in(&buf);
	size_t size = buf.size();
	de.parse(in, size);
	return size;
}

size_t pifn::streambuf_render(const serialisable& sr, streambuf& buf)
{
	std::ostream out(&buf);
	return sr.render(out);
}
