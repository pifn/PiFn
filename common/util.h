#ifndef _UTIL_H
#define _UTIL_H

#include <sstream>
#include <system_error>

/*
	Makes a string. e.g.

	int i = ...
	MKSTR("i = " << i)
*/
#define MKSTR(args) (((std::stringstream*)&(std::stringstream() << args))->str())

/*
 * Shorthand for the system exception which should be thrown after C type function
 * sets errno.
 */
#define ERRNO_EXCEPT(args) std::system_error(errno, std::system_category(), MKSTR(args));

#endif

