#ifndef _PQUEUE_H
#define _PQUEUE_H

#include "packet.h"

#include <boost/thread.hpp>
#include <queue>
#include <set>

namespace pifn
{
	using boost::mutex;
	using boost::lock_guard;

	/**
	 * A pausable and closable queue designed for single consumer and multiple
	 * producers.
	 *
	 * The queue contains a set of pproducer objects. When the number of
	 * elements in the queue exceeds or drops below the threshold then all the
	 * pproducers are paused or resumed respectivly. This is to notify them to
	 * suspend or continue producing.
	 *
	 * The queue can be closed from each end. Conceptually the producer has the
	 * back of the queue and the consumer has the front. Closing the front of the
	 * queue (by calling close_front()) discards all elements in the queue and
	 * notifies all the pproducers. Closing the back of the queue (by calling
	 * close_back()) notifies all the pproducers but only notifies the consumer
	 * if the queue is empty. This allows the consumer to consume all the
	 * elements in the queue if it is closed from the producer end.
	 *
	 * The consumer is notified:
	 * - when an element is placed on the queue when the queue was empty
	 * - when an element is poped from the queue and there are still at
	 *   lease one item on the queue
	 *
	 * The consumer notifier function and the functions of the pproducer are
	 * called during the call to pqueue functions. Care must be take that the
	 * pqueue is not re-entered.
	 */
	class pproducer
	{
	public:
		virtual void pause() = 0;
		virtual void resume() = 0;
		virtual void queue_closed() = 0;
	};

	template<typename T>
	class pconsumer
	{
	public:
		virtual void pqc_next(const std::shared_ptr<T>& element) = 0;
		virtual void pqc_closed() = 0;
	};

	template<typename T>
	class basic_pqueue
	{
	public:
		typedef std::shared_ptr<T> ppkt;
		typedef typename std::queue<ppkt>::const_reference const_reference;
		typedef typename std::queue<ppkt>::reference reference;

	private:
		mutable boost::mutex _lock;
		std::queue<ppkt> _queue;
		const size_t _threshold;
		bool _closed;

		std::multiset<pproducer*> _pproducers;

		pconsumer<T>* _pconsumer;
		bool _pconsumer_notified;

	public:
		basic_pqueue(size_t threshold = 5) :
		_threshold(threshold),
		_closed(false),
		_pconsumer(NULL),
		_pconsumer_notified(false)
		{
		}

		/**
		 * Returns a reference to the front of the queue. Behaviour is
		 * undefined if the queue is empty.
		 */
		const_reference front() const
		{
			lock_guard<mutex> guard(_lock);
			return _queue.front();
		}

		/**
		 * Returns a reference to the front of the queue. Behaviour is
		 * undefined if the queue is empty.
		 */
		reference front()
		{
			lock_guard<mutex> guard(_lock);
			return _queue.front();
		}

		/**
		 * If the queue is not empty, the element at the front is assigned to
		 * next and false is returned.
		 *
		 * If the queue is empty then next is reset and true is returned if the
		 * queue is closed.
		 */
		bool front(ppkt& next)
		{
			next.reset();

			lock_guard<mutex> guard(_lock);
			if(_queue.empty())
				return _closed;

			next = _queue.front();
			return false;
		}

		/*
		 * No back() functions are provided. With multiple producers it is
		 * not known if an item is still at the back of the queue once push()
		 * has returned.
		 */

		/**
		 * Closes the queue and discards any elements in the queue.
		 *
		 * The consumer is notified if any elements are discarded.
		 *
		 * The producers are notified if the queue was not already closed.
		 */
		void close_front()
		{
			lock_guard<mutex> guard(_lock);
			bool was_closed = _closed;
			bool was_empty = _queue.empty();
			while(!_queue.empty())
				_queue.pop();

			_closed = true;

			if(was_empty)
				cnotify();

			if(!was_closed)
				pnotify(call_close);
		}

		/**
		 * Closes the queue. If the queue is already closed then no further
		 * action is taken.
		 *
		 * The consumer is notified if the queue is empty.
		 * All the producers are notified.
		 */
		void close_back()
		{
			lock_guard<mutex> guard(_lock);
			if(_closed)
				return;

			_closed = true;
			if(_queue.empty())
				cnotify();

			pnotify(call_close);
		}

		/**
		 * Pushes an element onto the queue if it is not closed. If it is
		 * closed, does nothing.
		 *
		 * If adding this element exceeds the threshold, all the producers are
		 * paused. If if the queue was empty, the consumer is notified.
		 *
		 * Returns true if the queue is closed.
		 */
		bool push(ppkt&& packet)
		{
			lock_guard<mutex> guard(_lock);
			if(_closed)
				return true;

			const size_t sz = _queue.size();
			if(sz == _threshold)
				pnotify(call_pause);
			_queue.push(std::move(packet));

			if(sz == 0)
				cnotify();

			return false;
		}

		/**
		 * Pushes an element onto the queue if it is not closed. If it is
		 * closed, does nothing.
		 *
		 * If adding this element exceeds the threshold, all the producers are
		 * paused. If if the queue was empty, the consumer is notified.
		 *
		 * Returns true if the queue is closed.
		 */
		bool push(const ppkt& packet)
		{
			lock_guard<mutex> guard(_lock);
			if(_closed)
				return true;

			const size_t sz = _queue.size();
			if(sz == _threshold)
				pnotify(call_pause);
			_queue.push(packet);

			if(sz == 0)
				cnotify();

			return false;
		}

		/**
		 * Removes an element from the front of the queue if there are any to be
		 * removed.
		 *
		 * Returns true if the queue is not empty after poping an item.
		 */
		bool pop()
		{
			lock_guard<mutex> guard(_lock);
			_pconsumer_notified = false;

			if(!_queue.empty())
				local_pop();

			if(!_queue.empty())
				cnotify();

			return !_queue.empty();
		}

		/**
		 * Add a pproducer to the set that are notified.
		 */
		void add_producer(pproducer* producer)
		{
			lock_guard<mutex> guard(_lock);
			if(_queue.size() > _threshold)
				producer->pause();
			_pproducers.insert(producer);
		}

		/**
		 * Remove a pproducer from the set that are notified.
		 */
		void remove_producer(pproducer* producer)
		{
			lock_guard<mutex> guard(_lock);
			auto it = _pproducers.find(producer);
			if(it != _pproducers.end())
			{
				if(_queue.size() > _threshold)
					(*it)->resume();
				_pproducers.erase(it);
			}
		}

		void set_consumer(pconsumer<T>* consumer = NULL)
		{
			lock_guard<mutex> guard(_lock);
			_pconsumer = consumer;
		}

	private:
		//Only call the private functions if _lock is held

		void pnotify(void (*func)(pproducer&))
		{
			pproducer* last = NULL;
			for(pproducer* pp : _pproducers)
				if(last != pp)
				{
					func(*pp);
					last = pp;
				}
		}

		static void call_pause(pproducer& p)
		{
			p.pause();
		}

		static void call_resume(pproducer& p)
		{
			p.resume();
		}

		static void call_close(pproducer& p)
		{
			p.queue_closed();
		}

		void cnotify()
		{
			if(_closed && _queue.empty())
				_pconsumer->pqc_closed();
			else if(_pconsumer && !_pconsumer_notified)
			{
				_pconsumer_notified = true;
				_pconsumer->pqc_next(_queue.front());
			}
		}

		void local_pop()
		{
			_queue.pop();
			if(!_closed && _queue.size() == _threshold)
				pnotify(call_resume);
		}
	};

	typedef basic_pqueue<packet> pqueue;
}

#endif
