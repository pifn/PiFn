#ifndef _PACKET_H
#define _PACKET_H

#include <boost/asio/streambuf.hpp>
#include <boost/utility.hpp>
#include <memory>
#include <serialise.h>

namespace pifn
{
	class packet_header : public serialisable, public deserialisable
	{
	private:
		uint16_t _channel;
		uint16_t _length;

	public:
		static const size_t rsize = sizeof(_channel) + sizeof(_length);

		packet_header();
		packet_header(const packet_header& that);
		packet_header(uint16_t channel, uint16_t length);
		packet_header(std::istream& in);

		uint16_t channel() const;
		void channel(uint16_t channel);
		uint16_t length() const;
		void length(uint16_t length);
		void parse(std::istream& in, size_t size = rsize);
		size_t render(std::ostream& out) const;
	};

	class packet
	{
	public:
		uint16_t channel;
		boost::asio::streambuf payload;

		packet(uint16_t channel);
	};
}

#endif
