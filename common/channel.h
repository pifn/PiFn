#ifndef _CHANNEL_H
#define _CHANNEL_H

#include "async_stream.h"
#include "packet.h"
#include "serialise.h"

#include <map>
#include <string>

#define CR_ACTION_FAIL 0x01
#define CR_ACTION_CREATE 0x02
#define CR_ACTION_CLOSE 0x04

namespace pifn {
	class channel_action : public serialisable, public deserialisable
	{
	public:
		uint8_t action;
		uint16_t target;
		std::string type;
		std::string type_data;

		static const size_t TYPE_SIZE;
		static const size_t MIN_SIZE = sizeof(action) + sizeof(target);

		channel_action();
		channel_action(channel_action&& that);
		channel_action(const channel_action& that);
		channel_action(uint8_t action0, uint16_t target0, std::string type0 = "", std::string type_data0 = "");
		channel_action(std::istream& in, size_t size);

		channel_action& operator=(const channel_action&);
		channel_action& operator=(channel_action&&);

		void parse(std::istream& in, size_t size);
		size_t render(std::ostream& out) const;
	};
}
#endif
