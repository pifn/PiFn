#ifndef _ASYNC_STREAM_H
#define _ASYNC_STREAM_H

#include "action.h"
#include "inspect.h"
#include "packet.h"
#include "pqueue.h"
#include "serialise.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/system/error_code.hpp>
#include <boost/utility.hpp>
#include <memory>

namespace pifn
{
	using boost::asio::streambuf;
	using boost::asio::const_buffer;
	using boost::system::error_code;
	using std::function;
	using std::make_shared;
	using std::shared_ptr;
	using std::string;
	using std::unique_ptr;
	using std::vector;

	class async_stream_consumer;

	/**
	 * Interface for handling incomming packets
	 */
	class packet_handler
	{
	public:
		virtual ~packet_handler() {}

		/**
		 * Handle the packet. By defalt the packet object only remains in scope
		 * until the handler returns. If the handler wishes to take ownership
		 * of the packet it should create a shared_ptr from it.
		 */
		virtual void handle_packet(const shared_ptr<packet>& packet) = 0;
	};

	typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

	class rendered_packet
	{
	private:
		const shared_ptr<packet> _packet;
		streambuf _header;
		vector<const_buffer> _buffers;
		function<void(const error_code&, const size_t)> _handler;

	public:
		rendered_packet(const shared_ptr<packet>&, function<void(const error_code&, const size_t)>);
		rendered_packet(const rendered_packet&);

		inline const vector<const_buffer>& buffers() const
		{return _buffers; }
		inline packet& get_packet() const
		{return *_packet; }
		inline void handle(const error_code& ec, const size_t sz) {_handler(ec, sz); }

	private:
		void make_buffers();
	};

	/**
	 * A bidirectional stream. Sending is done withe the write_*() methods. To receive,
	 * read_packet() is called which will ensure that either io_error() or handle_packet()
	 * is called later. Only once one of these two has been called can read_packet() be
	 * called again. The pointer passed to handle_packet may be used until read_packet()
	 * or read_packet_used() is called.
	 */
	class async_stream :
		protected virtual packet_handler,
		private pauseable_action
	{
		friend class async_stream_consumer;

	private:
		const uint32_t _connection_number;

		//_writeq is declared before _consumer as must be destructed before it
		basic_pqueue<rendered_packet> _writeq;
		unique_ptr<async_stream_consumer> _consumer;

		streambuf _header;
		shared_ptr<packet> _packet;

	protected:
		ssl_socket _socket;

	public:
		async_stream(const async_stream&) = delete;
		async_stream& operator=(const async_stream&) = delete;
		async_stream(boost::asio::io_service& io_service, boost::asio::ssl::context& ssl_context, uint32_t connection_number);
		virtual ~async_stream();

		/**
		 * Starts the packet receive sequence. Packets will be read continiosly while the
		 * read cycle is not paused. This method should only be called once.
		 */
		void start_read();

		/**
		 * Write a serialisabel object to a channel. The object is serialised during
		 * this call and need not exist after the call returns
		 */
		void write_packet(const serialisable&, uint16_t channel);

		/**
		 * Write a packet. A copy of the shared pointer is kept until the io completes
		 * ensuring that the packet continues to exist while the io operation is
		 * in progress.
		 * The handler takes no arguments and is invoked only after a successful io
		 * operation. This function wraps the provided handler and calls write_packet_io().
		 */
		void write_packet(const shared_ptr<pifn::packet>& packet, std::function<void()> handler = NULL);

		virtual void shutdown() = 0;

		inline boost::asio::io_service& get_io_service() {return _socket.get_io_service(); };

		void close_writeq() {_writeq.close_back(); }
		void add_producer(pproducer* producer) {_writeq.add_producer(producer); }
		void remove_producer(pproducer* producer) {_writeq.remove_producer(producer); }
		pproducer* read_pauser() { return this; }

	protected:
		virtual void io_error(const string& where, const error_code& ec) = 0;

		class token {};
		virtual shared_ptr<token> make_token() {return make_shared<token>();}
		virtual void action();
	private:
		void handle_header(const error_code& ec, const size_t txsz);
		void read_payload(const uint16_t size);
		void handle_payload(const uint16_t size, const error_code& ec, size_t txsz);
	};

	class async_stream_consumer : private pconsumer<rendered_packet>, public boost::noncopyable
	{
		friend class async_stream;

	private:
		async_stream& _stream;

		async_stream_consumer(async_stream&);

	public:
		virtual ~async_stream_consumer();
		virtual void pqc_next(const std::shared_ptr<rendered_packet>& element);
		virtual void pqc_closed();
	};
};

#endif
