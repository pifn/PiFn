#ifndef _SERIALISE_H
#define _SERIALISE_H

#include <boost/asio/streambuf.hpp>
#include <iostream>
#include <string>

namespace pifn {
	class serialisable
	{
	public:
		virtual size_t render(std::ostream&) const = 0;
	};

	class deserialisable
	{
	public:
		virtual void parse(std::istream&, size_t) = 0;
	};

	class serialiser
	{
	private:
		std::ostream& _out;
		size_t _count;

	public:
		serialiser(const serialiser&) = delete;
		serialiser& operator=(const serialiser&) = delete;

		serialiser(std::ostream& out);

		size_t count() const;

		size_t write(const serialisable&);
		void write(const void* data, const size_t);
		void write_uint8(const uint8_t data);
		void write_uint16(const uint16_t data);
		size_t write_string(const std::string& data, const size_t pad_to = 0);
	};

	class deserialiser
	{
	private:
		std::istream& _in;
		size_t _count;

	public:
		deserialiser(const deserialiser&) = delete;
		deserialiser& operator=(const deserialiser&) = delete;

		deserialiser(std::istream& in);

		size_t count() const;

		void read(void* data, const size_t size);
		void read(deserialisable&, const size_t);
		uint8_t read_uint8();
		uint16_t read_uint16();
		std::string read_string(size_t size, bool trim_padding = true);
	};

	size_t streambuf_parse(deserialisable& de, boost::asio::streambuf& buf);
	size_t streambuf_render(const serialisable& sr, boost::asio::streambuf& buf);
}

#endif
