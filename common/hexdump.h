#ifndef _HEXDUMP_H
#define _HEXDUMP_H

#include <iostream>

extern size_t hex_block;
extern size_t hex_group;

void hexdump(std::ostream& out, size_t offset, const char* data, size_t length);

#endif

