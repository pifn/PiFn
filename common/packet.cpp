#include "packet.h"
#include "serialise.h"
#include "util.h"

#include <cassert>

using namespace pifn;
using namespace std;

packet_header::packet_header() :
_channel(0),
_length(0)
{
}

packet_header::packet_header(const packet_header& that) :
_channel(that._channel),
_length(that._length)
{
}

packet_header::packet_header(uint16_t channel, uint16_t length) :
_channel(channel),
_length(length)
{
}

packet_header::packet_header(istream& in)
{
	parse(in);
}

uint16_t packet_header::channel() const
{
	return _channel;
}

void packet_header::channel(uint16_t channel)
{
	_channel = channel;
}

uint16_t packet_header::length() const
{
	return _length;
}

void packet_header::length(uint16_t length)
{
	_length = length;
}

void packet_header::parse(istream& in, size_t size)
{
	assert(size == packet_header::rsize);
	(void)size; //size is unused if asserts are compiled out, triggering a warning
	deserialiser de(in);
	_channel = de.read_uint16();
	_length = de.read_uint16();
}

size_t packet_header::render(ostream& out) const
{
	serialiser sr(out);
	sr.write_uint16(_channel);
	sr.write_uint16(_length);
	return sr.count();
}

packet::packet(uint16_t channel0) :
channel(channel0)
{
}
