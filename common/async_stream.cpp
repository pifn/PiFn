#include "async_stream.h"

#include <iostream>
#include <vector>

using namespace pifn;
using namespace boost::asio;
using boost::asio::const_buffer;
using boost::asio::streambuf;
using std::function;
using std::istream;
using std::make_shared;
using std::ostream;
using std::shared_ptr;
using std::vector;

rendered_packet::rendered_packet(
	const shared_ptr<packet>& packet,
	function<void(const error_code&, size_t)> handler
) :
_packet(packet),
_header(packet_header::rsize),
_handler(handler)
{
	make_buffers();
}

rendered_packet::rendered_packet(const rendered_packet& that) :
_packet(that._packet),
_header(packet_header::rsize),
_handler(that._handler)
{
	make_buffers();
}

void rendered_packet::make_buffers()
{
	packet_header header(_packet->channel, _packet->payload.size());

	ostream hout(&_header);
	header.render(hout);

	_buffers.push_back(_header.data());
	_buffers.push_back(_packet->payload.data());
}

async_stream_consumer::async_stream_consumer(async_stream& stream) :
_stream(stream)
{
	_stream._writeq.set_consumer(this);
}

async_stream_consumer::~async_stream_consumer()
{
	_stream._writeq.set_consumer();
}

void async_stream_consumer::pqc_next(const std::shared_ptr<rendered_packet>& packet)
{
	shared_ptr<async_stream::token> t = _stream.make_token();
	async_write(
		_stream._socket,
		packet->buffers(),
		[this, t, packet](const error_code& ec, size_t txsz)
		{
			if(txsz > packet_header::rsize)
			{
				size_t consumed = txsz - packet_header::rsize;
				packet->get_packet().payload.consume(consumed);
				txsz -= packet_header::rsize;
			} else
				txsz = 0;

			_stream._writeq.pop();
			packet->handle(ec, txsz);
		}
	);
}

void async_stream_consumer::pqc_closed()
{
	_stream.shutdown();
}

async_stream::async_stream(io_service& io_service, ssl::context& ssl_context, uint32_t connection_number) :
_connection_number(connection_number),
_writeq(),
_header(packet_header::rsize),
_socket(io_service, ssl_context)
{
	_consumer.reset(new async_stream_consumer(*this));
}

async_stream::~async_stream()
{
}

void async_stream::start_read()
{
	trigger();
}

void async_stream::write_packet(const serialisable& sr, uint16_t channel)
{
	shared_ptr<packet> pkt(new packet(channel));
	streambuf_render(sr, pkt->payload);
	write_packet(pkt);
}

void async_stream::write_packet(const shared_ptr<packet>& packet, function<void()> handler)
{
	shared_ptr<rendered_packet> rp = make_shared<rendered_packet>(
		packet,
		[this, handler](const error_code& ec, const size_t txsz)
		{
			(void)txsz;
			if(ec)
				io_error("writing packet", ec);
			else if(handler)
				handler();
		}
	);
	packet_inspect(_connection_number, INSFLAG_SEND, rp->buffers());
	_writeq.push(move(rp));
}

void async_stream::action()
{
	assert(_packet.get() == NULL);
	_header.consume(_header.size());
	shared_ptr<token> t = make_token();
	async_read(
		_socket,
		_header,
		transfer_exactly(packet_header::rsize),
		[t, this](const boost::system::error_code& ec, const size_t txsz)
		{
			handle_header(ec, txsz);
		}
	);
}

void async_stream::handle_header(const boost::system::error_code& ec, const size_t txsz)
{
	if(ec)
	{
		io_error("reading header", ec);
		return;
	}
	else if(txsz == 0)
	{
		action();
		return;
	}

	assert(txsz == packet_header::rsize);
	assert(txsz == _header.size());

	istream hin(&_header);
	packet_header header(hin);
	assert(_header.size() == 0);

	assert(_packet.get() == NULL);
	_packet = make_shared<packet>(header.channel());
	read_payload(header.length());
}

void async_stream::read_payload(const uint16_t size)
{
	shared_ptr<token> t = make_token();
	async_read(
		_socket,
		_packet->payload,
		transfer_exactly(size),
		[t, this, size](const boost::system::error_code& ec, size_t txsz)
		{
			this->handle_payload(size, ec, txsz);
		}
	);
}

void async_stream::handle_payload(const uint16_t size, const boost::system::error_code& ec, size_t txsz)
{
	if(ec)
	{
		io_error("reading payload", ec);
		return;
	}
	else if(txsz == 0 && size != 0)
	{
		read_payload(size);
		return;
	}

	assert(txsz == size);
	assert(txsz == _packet->payload.size());

	packet_inspect(_connection_number, INSFLAG_NONE, _packet->channel, _packet->payload.data());
	const shared_ptr<packet> p(_packet);
	_packet.reset();

	handle_packet(p);
	trigger();
}
