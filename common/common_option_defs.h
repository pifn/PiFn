#ifndef _COMMON_OPTION_DEFS_H
#define _COMMON_OPTION_DEFS_H

/*
 * Definitions of prgram options and default values.
 * OPT_* option long name
 * DEC_* declaration, includes long name and an assosiaed short option if apropriate
 * DEF_* default values
 */

#define OPT_HELP "help"
#define DEC_HELP OPT_HELP

#define OPT_VERSION "version"
#define DEC_VERSION OPT_VERSION

#endif
