# Pi Functions

1. [Overview](../README.md)
1. [Intallation](install.md)
   1. [Building](build.md) and installing
   1. [Setting up the server](server_setup.md)
   1. [Setting up the client](client_setup.md)
1. Programs
   * [pifn](pifn.md)
   * [pifnd](pifnd.md)
1. [Protocol](protocol.md)
