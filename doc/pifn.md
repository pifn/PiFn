# pifn

Connects to the ```pifnd``` server, sends commands to it and outputs the responses.

## Usage

```
pifn [options] [directory_or_config_file]
```

## Description

```pifn``` makes a connection to the server (```pifnd```), sends commands to it from standard input and writes the responses from the server to standard output. If a translator command is provided, then all input is piped to the translator and the output of this is sent to the server. The translator is a way to provide more human-friendly commands and emit the required commands to ```pifnd``` in their place.

## Configuration File

The *directory_or_config_file* argument is the name of the configuration file to use, or the directory in which to look for a configuration file. If no directory of file name is provided the ```pifn``` will look in the current directory as if '.' had been specified. If the string ':' is given, ```pifn``` will not look for any configuration files.

If a directory is provided (or the default '.' used), a file named ```pifn.conf``` in that directory will be used. If the config file is specified (either by name of by directoy), it must exist.

Optionally a local config file will be used if it exists. This file has '-local' appended before the suffix.

The config file contains a list of options as key=value pairs using the names of the options below. Any options given on the command line will supersede any given in either config file. Options given in the local config file supersede those in the specified config file.

## Options

### Config Options

These options can be used on the command line or in a config file.

**--channel** type[:data]
	Specify a channel type to use. The the default channel is 'shell'

**--csr** file
	location of certificate sign request file

**-c** file
**--certificate**
	location of certificate file

**-h** host
**--host** host
	name of host to connect to

**-k file
**--key** file
	location of key file

**-L** localport[:remotehost[:remoteport]]
**--forward-port** localport[:remotehost[:remoteport]]
	Forward a local port to a remote host. ```pifn``` will bind to localport on the loopback interface of the local machine. Connections received on the port will cause a port forwarding channel to be opened to the remote host on the specified port. This allows connecting to a service behind a firewall. If no port number is specified the server will interpret the hostname as a symbolic name and decide where to connect to.

**-n**
**--no-create**
	Do not create a new key or csr file

**--no-connect**
	exit before connecting to remote host

**-p** port
**--port** port
	host port to connect to

**--openssl-conf** file
	openssl config file for key/csr generation

**-o** file
**--output** file
	output to file instead of STDOUT

**-t** script
**--translator** script
	run a script to translate input. The scripts STDIN and STDERR will be inherited from pifn and it's STDOUT will be sent to the server.

**-w**
**--remote-wait**
	Don't close the channel when the end of input is reached. Wait for the server to close the channel.

### Command Line Options

These options can only be used on the command line.

**--help**
	Display help and exit

**--version**
	show version and exit
