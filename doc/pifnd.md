# pifnd

Server that controls hardware.

## Usage

```
pifnd [-d] [-c config_file]
```

## Description

```pifnd``` listens for connections from ```pifn``` and executes the commands it receives.

## Options

**-d**
	Daemonise, detaches from the terminal and runs in the background.

**-c config_file**
	Configuration file to use
