# Server Setup

These instructions are for Debian

## 1. Build and Install

See the [build](build.md) page.

## 2. Update the Startup Scripts

Run `sudo update-rc.d pifnd`.

## 3. Start the Server

Run `sudo service pifnd start`

## 4. (Optionally) Disable Boot Time Startup

If you do not want to start the server each time the device boots, run `sudo update-rc.d pifnd disable`


