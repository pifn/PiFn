# Building

These are instructions on how to build Pi Functions from source.

## 1. Get the sources

Clone from git repository:

```
git clone https://github.com/xianic/pifn.git
cd pifn
```

## 2. Install the Dependencies

Run time dependencies are:

* Boost
* LibBSD
* OpenSSL

Build time dependencies are:

* Autoconf
* Automake
* Libtool

These can be installed with:

```
sudo apt-get install libboost-all-dev libssl-dev libbsd-dev autoconf automake libtool check pkg-config
```

## 3. Boostrap the build

Generate the configure script:

```
autoreconf --install
```

## 4. Configure Build

```
./configure --localstatedir=/var --sysconfdir=/etc
```

## 5. Build

If you have multiple cores available on your system, you can use the ```-j``` switch to make use of them. To build one job at a time:
```
make
```

To build four jobs at the same time e.g. if you have a Rasperry Pi 2 with 4 cores:
```
make -j4
```

## 6. Install

```
sudo make install
```
