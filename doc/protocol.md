# Protocol

This is a description of the protocol between that is used to communicate with the ```pifnd``` server.

## TCP Connecion

The connection to the ```pifnd``` server uses SSL with a client certificate for authentication. After the SSL handshake is complete both the server and the client will send 64 bytes of null padded ASCII text identifying their protocol version implementation types. These 64 will contain at least two single space separated fields, the first being the protocol version and the second being the implementation type. The protocol version is "p1" as described below. The zero or more remaining fields shall be ignored.

On top of the SSL is the idea of channels: a channel is a bi-directional stream of bytes. To achieve this the data is split into packets, consisting of a header and a payload. The header identifies which channel the data is part of and the length of the payload. The payload immediately follows the packet.

Header format:

Field Name | Type | Description
:--------- | :--- | :----------
channel | uint16_t | Channel number that packet is part of
length  | uint16_t | Length of the payload in bytes

Immediatly after the connection is established, channel 0 is the only valid channel.

## Management Channel

Channel 0 is used to for creating and destroying other channels. All packets follow the same format as below. The client makes requests which the server replys to. The client must not reply to the server.

### Packet Format

Field Name     | Type         | Description
:------------- | :----------- | :----------
target_channel | uint16_t     | Channel number that packet is part of
action         | uint8_t      | 0x02 for create, 0x04 for destory
channel_type   | char[7]      | type name (null padded)
create_data	   | type-defined | Data passed to the type of channel when it is created

The least significant bit of the action field is used to indicate success or failure. This is always zero in requests to the server, and is zero to indicate successful request and 1 to indicate failure to satisy a request.

The target_channel is the number of the channel that this request applies to. For requests to create a channel this should be a number that is not already associated with a channel. For requests to destroy a channel, this should be the number of an existing channel except 0.

The channel_type and create_data data fields are optional. The length of the packet need only be long enough to accomodate the data needed. e.g. If only the first 3 bytes of the channel_type field are used, the packet may be 6 bytes long. If any data is present in the create_data field, then the channel_type field must be present and filled with nulls if not used.

### Channel Usage

Both requests and responses follow the same format as above. To create a channel the client sends a request, and the server will responed with either success or failure. If the reponse indicates success the channel has been created an the client may use it. If the response indicates failure then the channel as not been created, and details of the failure may be present int the channel_type and create_data fields.

Once a channel has been created, either the client or the server may close it by the notifying the other. If the server recieves a close channel request it must acknowedge it.


### Examples

Creating and destroying the 'shell' channel form client's point of view. s = send, r = recievce:

#### Client closes channel

s0: 00 01 02 73 68 65   6C 6C                                  ...she ll
Client send request to create shell on channel 0. The create_data field is empty and so the channel_type field can be truncated to the size of the data.

r0: 00 01 02                                                   ...
Server responds, successfully created channel 1

s1: 68 65 6C 70 0A                                             help.
r1: 69 6E 66 6F 3A 20   43 6F 6D 6D 61 6E   64 73 20 61 76 61  info:  Comman ds ava
    ...
Channel 1 in use

s0: 00 01 04                                                   ...
Request to close channel 1

r0: 00 01 04                                                   ...
Channel 1 closed

#### Server closes channel

s0: 00 01 02 73 68 65   6C 6C                                  ...she ll
r0: 00 01 02                                                   ...
s1: 71 75 69 74 0A                                             quit.
r0: 00 01 04                                                   ...

## Shell Channel

Channel name is 'shell'. It does not take or produce any type data when created.

The following commands are accepted:

Command | Action
:--------------- | :----------
drive &lt;motor number&gt; &lt;speed&gt; [&lt;timeout&gt;] | Drive the motor at given speed (0 to 7, 0 for stop)
servo &lt;output number&gt; &lt;position&gt; | drive a servo to a position. Position is a decimal number from -1 to 1 with 0 being in the centre.
pwm &lt;output number&gt; &lt;duty cycle&gt; | Turn output on or off. Duty cycle is is a decimal number from 0 (off) to 1 (fully on) that denotes how strongly on or off it should be.
led &lt;red&gt; &lt;green&gt; &lt;blue&gt; | Set to colour of the LED on the Navio board. Use decimal values from 0 to 1

There are 8 motor and 13 pwm outputs. The ```drive``` command operates the motors and the ```pwm``` and ```servo``` commands both operate the same pwm outputs.

### drive &lt;motor number&gt; &lt;speed&gt; [&lt;timeout&gt;]

This drive one of the 8 motors at the given speed. Speed an integer between 7 and -7 inclusive with 0 being stop. Motors are numbered 0 to 7.

The timeout argument is option a decimal number of seconds after which the motor will be stopped. If a timeout has been set for a motor and another drive command arrives for the motor, then the first timeout is discarded, the timeout for the second command used if it has one.

### servo &lt;output number&gt; &lt;position&gt;

Output a signal on one of the pwm outputs suitable for driving a servo. The position is a decimal number from -1 to 1 which correspond to each end of the servo's range. 0 is the center.

### pwm &lt;output number&gt; &lt;duty cycle&gt;

Output a pwm signal. The duty cycle is a decimal number from 0 to 1 that corresponds to fully off or fully on respectively. e.g. ```pwm 3 0.5``` sets output 3 to 50% on.

### led &lt;red&gt; &lt;green&gt; &lt;blue&gt;

Sets the the colour and brightness of the led on the navio board. The red, green and blue values are decimal numbers from 0 to 1.

## Echo and Null Channels

Channel name are 'echo' and 'null' respectivly. The echo channel ends back any packets sent to it, and the null channel ignores all packets it receives and does not send any. These are intended mainly for debugging.

## Port forwarding channel

The prot forwarding channel opens a TCP connection from the server to another host. The name of the channel is 'fport' and the data to send on opening is the remote endpoint name in the format host[:port]. If the remote endpoint does not contain a port number the host is interpreted as a symbolic name and the server will decide where the remote end of the channel will connect. The server may refuse to open portforwarding channels, particularly for arbitary hosts.
