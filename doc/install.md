# Installation

Pi Functions needs to be installed on both the client and the server devices. These may be the same device, but if not you will need to build and install on both devices. The overview of the proces is:

1. [Build](build.md) and install Pi Functions on both devices
1. [Set up the server](server_setup.md)
1. [Set up the client](client_setup.md)
