#ifndef _OPTIONS_H
#define _OPTIONS_H

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

class options {
public:
	options(const options&) = delete;
	void operator=(const options&) = delete;

	options();
	virtual ~options();

	void parse_command_line(int argc, char* argv[]);
	void parse_file(const boost::filesystem::path& file);

	bool show_help();

	template<typename T> const T& get(const std::string& name) const {
		return _vm[name].as<T>();
	}
	bool has(const std::string& name) const;

private:
	boost::program_options::options_description _general;
	boost::program_options::options_description _config;
	boost::program_options::options_description _hidden;
	boost::program_options::positional_options_description _positional;

	boost::program_options::options_description _config_file;
	boost::program_options::options_description _visible;
	boost::program_options::options_description _cmd_line;

	boost::program_options::variables_map _vm;
};

#endif
