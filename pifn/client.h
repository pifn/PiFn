#ifndef _CLIENT_H
#define _CLIENT_H

#include "async_stream.h"
#include "certificate.h"
#include "channel.h"
#include "config.h"

#include <string>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/thread.hpp>
#include <memory>
#include <vector>

#define IMPLEMENTATION_NAME "pifn-" PACKAGE_VERSION

/**
 * The stage of a channels lifecycle is marked by it's state.
 * INIT: Has been created
 * OPENING: open() called but server has yet to respond
 * FAILED: Creation failed or clsoed by server
 * ACTIVE: Server has confirmed channel is open, channel is fully operational
 * CLOSING: Server has been asked to close the channel but has not yet
 *          acknowledged.
 * CLOSED:  Server has closed the channel
 *
 * INIT
 *   |
 *   | open() asks server to open channel
 *  \|/
 * OPENING --> Server says no --> FAILED
 *   |
 *   | Server confirms open
 *  \|/
 * ACTIVE --> Server sends close notification --> FAILED
 *   |
 *   | shutdown() asks server to close channel
 *  \|/
 * CLOSING
 *   |
 *   | Server sends close notifaction
 *  \|/
 * CLOSED
 */
enum channel_state { INIT, OPENING, ACTIVE, CLOSING, CLOSED, FAILED };

class client;

class client_channel : public virtual pifn::packet_handler
{
	friend class client_channel_manager;
	friend class client;

protected:
	boost::mutex _lock;

	/*
	 * The id of a channel identifies it to the server. There may be more IDs in
	 * use by the client than the server knows about, typicaly when channels
	 * have been created and registered but not yet opened.
	 */
	const uint16_t _id;
	const std::weak_ptr<client> _client;
	channel_state _ch_state;

public:
	client_channel(const client_channel&) = delete;
	client_channel& operator=(const client_channel&) = delete;

	/*
	 * Construct a channel, assign it a free number and add it to the client
	 */
	client_channel(const std::shared_ptr<client>&);

	/*
	 * Destruct the channel, removing it from the client
	 */
	virtual ~client_channel();

	uint16_t id() { return _id; }

	void open(const std::string& type, const std::string& data = "");
	channel_state get_state() {return _ch_state; }

	//Is channel in ACTIVE or CLOSING state
	bool is_running();
	static inline bool is_running(const channel_state state)
	{
		return state == OPENING || state == ACTIVE || state == CLOSING;
	}

	static inline bool is_starting(const channel_state old_state, const channel_state new_state)
	{
		return !is_running(old_state) && is_running(new_state);
	}

	static inline bool is_stopping(const channel_state old_state, const channel_state new_state)
	{
		return is_running(old_state) && !is_running(new_state);
	}

	/*
	 * Send request to server to close this channel, state is set to CLOSING
	 */
	void shutdown(std::shared_ptr<client_channel> = NULL);

protected:
	/*
	 * Called when state changes. The implementation here removes this channel
	 * from the client when it is no longer running. The fuction may be overidden
	 * to change or add to this behaviour.
	 */
	virtual void state_changed(channel_state old_state, channel_state new_state);

private:
	/*
	 * Construct a channel with the given id. The channel is not added to the
	 * client
	 */
	client_channel(const std::shared_ptr<client>&, int);

	/*
	 * Set the state of the channel. Returns the old state
	 */
	channel_state set_state(channel_state to);

	/*
	 * Set the state of the channel only if it is in the from state, otherwise
	 * does nothing. Returns the old state, whif if not equal to from, will not
	 * have changed.
	 */
	channel_state set_state(channel_state from, channel_state to);
};

class client_channel_manager : public client_channel
{
public:
	client_channel_manager(const std::shared_ptr<client>& client);

	void handle_packet(const std::shared_ptr<pifn::packet>& packet);
};

class client : public pifn::async_stream, public std::enable_shared_from_this<client>
{
private:
	std::map<uint16_t, client_channel*> _channels;
	uint16_t _next_channel;
	bool _ssl_active;
	std::unique_ptr<client_channel_manager> _manager;
	const certificate* _remote_cert;
	bool _manual_cert_check;

public:
	client(boost::asio::io_service& io_service, boost::asio::ssl::context& context, const certificate* remote_cert);
	virtual ~client() {};

	void connect(boost::asio::ip::tcp::resolver::iterator& endpoint_iterator);
	virtual void shutdown();
	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);
	void welcome();

	/*
	 * Adds a channel to the channel map and asigns a free channel number
	 */
	uint16_t add_channel(client_channel*);

	/*
	 * Adds the control channel the connection. This must be called after
	 * constructing a shared pointer to the connection and before it
	 * receives any packets
	 */
	void add_channel0();

	/*
	 * Return a handler for a channel number. If no handler for the given
	 * number is currently registered then null is returned.
	 */
	client_channel* get_channel(uint16_t);

	/*
	 * Remove a channel handler by number, returning the previously
	 * registered handler. The channel must be registered prior to this call.
	 */
	client_channel* remove_channel(uint16_t);

	/*
	 * Remove a channel if and only if it is registered. It is not an error to
	 * call this with an unregistered channel. Returns true if the channel was
	 * removed.
	 */
	bool remove_channel(client_channel*);

	void remove_all_channels();

protected:
	virtual void io_error(const std::string& where, const boost::system::error_code& ec);

private:
	uint16_t free_channel_number();

	bool verify_certificate(bool preverified, boost::asio::ssl::verify_context& ctx);

	void close();

	void show_error(const std::string& message, const boost::system::error_code& ec);
};

#endif
