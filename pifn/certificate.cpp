#include "certificate.h"

#include "fd_file.h"
#include "util.h"

#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include <iomanip>
#include <openssl/pem.h>
#include <stdio.h>

using namespace boost::filesystem;
using namespace boost::posix_time;
using namespace std;

static size_t asn1_time_strlen(const ASN1_TIME* time)
{
	switch(time->type)
	{
	case V_ASN1_UTCTIME:
		return 12;
	case V_ASN1_GENERALIZEDTIME:
		return 14;
	default:
		throw invalid_argument(MKSTR("unknow asn1 time type: " << time->type));
	}
}

static ptime asn1_time_parse(const ASN1_TIME* time)
{
	const size_t char_len = asn1_time_strlen(time);
	string stime((const char*)time->data, char_len);
	if(char_len == 12)
		stime = "20" + stime;
	stime = stime.substr(0, 8) + "T" + stime.substr(8);
	return ptime(from_iso_string(stime));
}

certificate::certificate(X509* cert, bool free) :
_cert(NULL),
_free(false)
{
	reset(cert, free);
}

certificate::certificate(const path& file) :
_cert(NULL),
_free(false)
{
	reset();
	load_file(file);
}

certificate::~certificate()
{
	reset();
}

bool certificate::operator==(const certificate& that) const
{
	return !X509_cmp(_cert, that._cert);
}

void certificate::reset(X509* cert, bool free)
{
	if(_cert && _free)
		X509_free(_cert);
	_cert = cert;
	_free = free;
}

void certificate::load_file(const path& file_path)
{
	path_file file(file_path.string(), O_CLOEXEC);
	reset(PEM_read_X509(file.fdopen("r"), NULL, NULL, NULL), true);
	if(!_cert)
		throw invalid_argument(MKSTR("unable to parse certificate in: " << file_path));
}

time_period certificate::validity_period() const
{
	return time_period(
		asn1_time_parse(X509_get_notBefore(_cert)),
		asn1_time_parse(X509_get_notAfter(_cert))
	);
}

int certificate::validate_time() const
{
	time_period valid_period = validity_period();
	ptime now = microsec_clock::universal_time();
	if(valid_period.contains(now))
		return 0;

	return valid_period.begin() > now ? -1 : 1;
}

string certificate::subject_one_line() const
{
	char subject_name[256];
	X509_NAME_oneline(X509_get_subject_name(_cert), subject_name, 256);
	return string(subject_name);
}

string certificate::sha1_string() const
{
	ostringstream out;
	out << hex << setfill('0') << uppercase;
	for(int i = 0; i < SHA_DIGEST_LENGTH; ++i)
	{
		if(i > 0)
			out << ":";
		out << setw(2) << static_cast<int>(_cert->sha1_hash[i]);
	}
	return out.str();
}

X509* certificate::native_handle() const
{
	return _cert;
}
