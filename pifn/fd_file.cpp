#include "fd_file.h"
#include "util.h"

#include <fcntl.h>
#include <cassert>
#include <system_error>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

fd_file::fd_file(fd_file&& that) :
_fd(that._fd),
_file(that._file)
{
	that._fd = -1;
	that._file = NULL;
}

fd_file::fd_file(int fd) :
_fd(fd),
_file(NULL)
{
}

fd_file::~fd_file()
{
	if(_file)
		fclose(_file);
}

FILE* fd_file::fdopen(const char* mode)
{
	assert(_file == NULL);
	_file = ::fdopen(_fd, mode);
	if(!_file)
		throw ERRNO_EXCEPT("fdopen");
	return _file;
}

path_file::path_file(path_file&& that) :
fd_file(move(that))
{
}

path_file::path_file(const string& path, int flags, mode_t mode) :
fd_file(open(path.c_str(), flags, mode))
{
	if(_fd < 0)
		throw ERRNO_EXCEPT("could not open file: " << path);
}

path_file::path_file(const string& path, int flags) :
fd_file(open(path.c_str(), flags))
{
	if(_fd < 0)
		throw ERRNO_EXCEPT("could not open file: " << path);
}

path_file::~path_file()
{
	if(_fd >= 0 && _file == NULL)
		close(_fd);
}
