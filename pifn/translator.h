#ifndef _TRANSLATOR_H
#define _TRANSLATOR_H

#include <stdio.h>

class translator {
public:
	translator(const translator&) = delete;
	void operator=(const translator&) = delete;

	translator() {};
	virtual ~translator() {};

	virtual int input_stream() {return STDIN_FILENO; };
	virtual void stop() {};

private:
};

#endif
