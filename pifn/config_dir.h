#ifndef _CONFIGDIR_H
#define _CONFIGDIR_H

#include "certificate.h"
#include "fd_file.h"
#include "options.h"
#include "translator.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/filesystem.hpp>
#include <memory>
#include <vector>

#define CONFIG_FILE_NAME "pifn.conf"
#define CONFIG_LOCAL_SUFFIX "-local"
#define KEY_SIZE "4096"
#define KEY_NAME "local.key"
#define CSR_NAME "local.csr"
#define CRT_NAME "local.pem"

class config_dir
{
private:
	boost::asio::ssl::context _ssl_context;
	boost::filesystem::path _base_dir;
	options _options;
	std::unique_ptr<const certificate> _remote_cert;

public:
	config_dir(const config_dir&) = delete;
	void operator=(const config_dir&) = delete;

	config_dir();
	virtual ~config_dir();

	/*
	 * Parse the command line options, executes optiones that have an imediate effect (e.g. help)
	 * and loads the config file if required.
	 * Returns true if all required actions have been completed and the program should exit, or false
	 * if the program should continue.
	 */
	bool init(int argc, char* argv[]);
	std::shared_ptr<translator> start_translator();
	boost::asio::ssl::context& ssl_context() {return _ssl_context; }

	std::string key_file() const;
	std::string certificate_file() const;
	boost::filesystem::path openssl_config() const;
	std::string connect_host() const;
	std::string connect_port() const;
	std::string channel_type() const;
	std::string channel_data() const;
	bool remote_wait() const;
	std::unique_ptr<fd_file> output_file() const;
	std::vector<std::string> fport_data() const;
	const std::unique_ptr<const certificate>& remote_certificate() const;

private:
	const boost::filesystem::path resolve_option(const std::string& option_name) const;
	void init_key();
};

#endif
