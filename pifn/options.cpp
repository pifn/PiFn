#include "config.h"
#include "options.h"
#include "option_defs.h"
#include "util.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

options::options() :
	_general("General options"),
	_config("Config options")
{
	_general.add_options()
		(DEC_HELP, "show help")
		(DEC_VERSION, "show version and exit")
		(DEC_NOCREATE, "do not create a new key or csr")
		(DEC_NOCONNECT, "exit before connecting to remote host")
		(DEC_OUTPUT, po::value<string>(), "output to file")
	;
	_config.add_options()
		(DEC_HOST, po::value<string>()->default_value(DEF_HOST), "host name to connect to")
		(DEC_PORT, po::value<string>()->default_value(DEF_PORT), "port to connect to")
		(DEC_KEY, po::value<fs::path>()->default_value(DEF_KEY), "SSL key")
		(DEC_CSR, po::value<fs::path>()->default_value(DEF_CSR), "SSL CSR")
		(DEC_CRT, po::value<fs::path>()->default_value(DEF_CRT), "SSL certificate")
		(DEC_RCRT, po::value<fs::path>(), "server certificate")
		(DEC_OPENSSLCONF, po::value<fs::path>()->default_value(DEF_OPENSSLCONF), "openssl config file")
		(DEC_TRANSLATOR, po::value<string>(), "input translater executable")
		(DEC_CHANNEL, po::value<string>()->default_value(DEF_CHANNEL), "channel type")
		(DEC_WAIT, "wait for server to close channel before exiting")
		(DEC_FPORT, po::value<vector<string>>(), "forward local port to remote host")
	;
	_hidden.add_options()
		(DEC_CONFIG, po::value<fs::path>()->default_value(DEF_CONFIG), "configuration file")
	;

	_positional.add(OPT_CONFIG, 1);

	_cmd_line.add(_general).add(_config).add(_hidden);
	_config_file.add(_config);
	_visible.add(_general).add(_config);
}

options::~options() {
}

void options::parse_command_line(int argc, char* argv[])
{
	po::store(po::command_line_parser(argc, argv).options(_cmd_line).positional(_positional).run(), _vm);
	po::notify(_vm);
}

void options::parse_file(const fs::path& file)
{
	ifstream ifs(file.c_str());
	if(!ifs)
		throw runtime_error(MKSTR("could not read file: " << file));

	store(parse_config_file(ifs, _config_file), _vm);
	notify(_vm);
}

bool options::show_help()
{
	bool exit_soon = false;

	if(has(OPT_VERSION))
	{
		cout << "Version: " << PACKAGE_VERSION << endl;
		exit_soon = true;
	}

	if(has(OPT_HELP))
	{
		cout << "Usage:" << endl
			<< "\tpifn [options] [config_file]" << endl
			<< endl
			<< "config_file: may be one of:" << endl
				<< "\t- absent, defaults to pifn.conf in the current directory" << endl
				<< "\t- path to a config file" << endl
				<< "\t- path to a directory containg a config file named pifn.conf" << endl
				<< "\t- ':' in which case no config file is read" << endl
			<< _visible << endl;
		exit_soon = true;
	}

	return exit_soon;
}

bool options::has(const string& name) const
{
	return _vm.count(name);
}
