#ifndef _CERTIFICATE_H
#define _CERTIFICATE_H

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/filesystem.hpp>
#include <openssl/x509.h>
#include <openssl/x509v3.h>

class certificate
{
private:
	X509* _cert;
	bool _free;

public:
	certificate(const certificate&) = delete;
	certificate& operator=(const certificate&) = delete;

	certificate(const boost::filesystem::path& file);
	certificate(X509* cert, bool free);
	virtual ~certificate();

	bool operator==(const certificate&) const;

	void reset() {reset(NULL, false);};
	void reset(X509* cert, bool free);

	void load_file(const boost::filesystem::path& file);

	/**
	 * Returns the time period for which the the certificate is valid,
	 * that is from its not before date until its not after date
	 */
	boost::posix_time::time_period validity_period() const;

	/**
	 * Test the notBefore and not_after fields and determin if the
	 * certificate is currently valid.
	 * return value:
	 *   -1    not yet valid
	 *   0     is currently valid
	 *   1     has expired
	 */
	int validate_time() const;

	X509* native_handle() const;
	std::string subject_one_line() const;
	std::string sha1_string() const;
};

#endif
