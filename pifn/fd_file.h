#ifndef _FILE_H
#define _FILE_H

#include <cstdio>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

class fd_file
{
public:
	fd_file(const fd_file&) = delete;
	const fd_file& operator=(fd_file&) = delete;
	fd_file(int fd);
	fd_file(fd_file&&);
	virtual ~fd_file();

	int fd() const {return _fd;}
	FILE* fdopen(const char*);

protected:
	int _fd;
	FILE* _file;
};

class path_file : public fd_file
{
public:
	path_file(path_file&&);
	path_file(const std::string& path, int flags, mode_t mode);
	path_file(const std::string& path, int flags);
	virtual ~path_file();
};

#endif
