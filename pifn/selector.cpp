#include "selector.h"
#include "util.h"

#include <algorithm>
#include <cassert>

#define SIG_INTR 0
#define SIG_STOP 1

using namespace std;
using boost::asio::io_service;
using boost::lock_guard;
using boost::mutex;

fdpipe::fdpipe(bool autoclose)
{
	int rt = pipe2(_fds, autoclose ? O_CLOEXEC : 0);
	if(rt)
		throw std::system_error(errno, std::system_category(), std::string("pipe2()"));
}

fdpipe::~fdpipe()
{
	close();
}

void fdpipe::close()
{
	if(_fds[0] >= 0 && _fds[1] >= 0) {
		::close(_fds[0]);
		::close(_fds[1]);
		_fds[0] = -1;
		_fds[1] = -1;
	}
}

selector::signaller::signaller(selector& selector) :
_selector(selector)
{
	selector.track(_sigpipe.read(), bind(&signaller::handler, this, _1));
	selector.track(_sigpipe.read(), INTR_READ);
}

void selector::signaller::signal(char sig)
{
	int ret = write(_sigpipe.write(), &sig, 1);
	if(ret < 0)
		throw ERRNO_EXCEPT("write()");
}

void selector::signaller::handler(int interest)
{
	assert((interest & ~INTR_CANCELED) == INTR_READ);
	if(interest & INTR_CANCELED)
		return;

	char sig;
	int rt = read(_sigpipe.read(), &sig, 1);
	_selector.track(_sigpipe.read(), INTR_READ, false);
	if(rt < 0)
		throw ERRNO_EXCEPT("read()");
	if(rt == 0)
		cerr << "unexpected end of pipe" << endl;
	else
	{
		switch(sig)
		{
			case SIG_INTR: //Interrupt
				break;
			case SIG_STOP: //Stop
				{
					lock_guard<mutex> guard(_selector._lock);
					_selector._stopped = true;
				}
				return;
			default:
				throw runtime_error(MKSTR("Unknown signal " << (int)sig));
		}
	}
}

selector::selector() :
_stopped(false),
_signaller(*this),
_counter([this](){_signaller.signal(SIG_STOP);})
{
}

void selector::track(int fd, int interest, bool interrupt_selector)
{
	if(stopped())
		notify(fd, INTR_CANCELED | interest);
	else
	{
		lock_guard<mutex> guard(_lock);
		_handlers[fd].interest = interest;
		if(interrupt_selector)
			_signaller.signal(SIG_INTR);
	}
}

void selector::track(int fd, select_handler handler)
{
	lock_guard<mutex> guard(_lock);
	_handlers[fd].handler = handler;
}

void selector::untrack(int fd)
{
	tracker t;

	{
		lock_guard<mutex> guard(_lock);
		auto it = _handlers.find(fd);
		if(it != _handlers.end())
		{
			t = it->second;
			_handlers.erase(it);
		}
	}

	if(t.handler && t.interest)
		t.handler(INTR_CANCELED | t.interest);
}

token selector::work()
{
	return token(_counter);
}

bool selector::stopped()
{
	lock_guard<mutex> guard(_lock);
	return _stopped;
}

void selector::notify(int fd, int interest)
{
	select_handler handler;

	{
		lock_guard<mutex> guard(_lock);

		tracker& t(_handlers[fd]);
		t.interest &= ~(t.interest | INTR_CANCELED);
		handler = t.handler;
	}

	if(interest && handler)
		handler(interest);
}

void selector::run() {
	fd_set fd_read;
	fd_set fd_write;

	while(!stopped())
	{
		FD_ZERO(&fd_read);
		FD_ZERO(&fd_write);

		//Build sets
		int maxfd = 0;
		{
			lock_guard<mutex> guard(_lock);
			for(auto it = _handlers.begin(); it != _handlers.end(); ++it)
			{
				if(it->second.interest & INTR_READ)
				{
					FD_SET(it->first, &fd_read);
					maxfd = max(maxfd, it->first);
				}
				if(it->second.interest & INTR_WRITE)
				{
					FD_SET(it->first, &fd_write);
					maxfd = max(maxfd, it->first);
				}
			}
		}

		//Select
		int retval = select(maxfd + 1, &fd_read, &fd_write, NULL, NULL);
		if(retval < 0)
			throw ERRNO_EXCEPT("select()");

		//Call handlers
		for(int fd = 0; fd <= maxfd; ++fd)
		{
			if(FD_ISSET(fd, &fd_read))
				notify(fd, INTR_READ);
			if(FD_ISSET(fd, &fd_write))
				notify(fd, INTR_WRITE);
		}
	}

	//notify canceled handlers
	fd_map handlers(_handlers);
	{
		lock_guard<mutex> guard(_lock);
		_handlers.clear();
	}
	for(auto it = handlers.begin(); it != handlers.end(); ++it)
		if(it->second.interest && it->second.handler)
			it->second.handler(INTR_CANCELED | it->second.interest);
}
