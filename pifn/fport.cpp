#include "fport.h"
#include "util.h"

#include "protocol.h"

using namespace pifn;
using namespace boost::system;
using boost::asio::ip::tcp;
using boost::asio::buffer_cast;
using boost::asio::error::misc_errors;
using std::cerr;
using std::endl;
using std::invalid_argument;

static const size_t READ_SIZE = 2048;

fport_listener::fport_listener(const shared_ptr<client>& client, const string& data) :
_client(client),
_acceptor(client->get_io_service())
{
	client->add_producer(this);
	string local_port;
	try
	{
		size_t sep = data.find(PATH_SEPARATOR);
		if(sep == string::npos)
			throw invalid_argument("no '" PATH_SEPARATOR "'");
		local_port = data.substr(0, sep);
		_remote_data = data.substr(sep + 1);
		cerr << "forwarding local port " << local_port << " to " << _remote_data << endl;
	}
	catch(const invalid_argument& ex)
	{
		throw invalid_argument(MKSTR("port forward data must be in format 'local_port" PATH_SEPARATOR "host:remote_port': " << ex.what()));
	}

	tcp::resolver resolver(client->get_io_service());
	tcp::endpoint endpoint = *resolver.resolve({"localhost", local_port});
	_acceptor.open(endpoint.protocol());
	_acceptor.set_option(tcp::acceptor::reuse_address(true));
	_acceptor.bind(endpoint);
	_acceptor.listen();

	accept();
}

fport_listener::~fport_listener()
{
	if(auto client = _client.lock())
		client->remove_producer(this);
}

void fport_listener::accept()
{
	if(!_acceptor.is_open())
		return;

	auto client = _client.lock();
	if(!client)
		return;

	shared_ptr<fport_connection> con = make_shared<fport_connection>(client);
	_acceptor.async_accept(
		con->_socket,
		[this, con](const boost::system::error_code& ec)
		{
			if(!ec)
			{
				con->open(_remote_data);
				accept();
			}
		}
	);
}

void fport_listener::action()
{
}

void fport_listener::queue_closed()
{
	_acceptor.cancel();
}

fport_connection::fport_connection(const shared_ptr<client>& client) :
client_channel(client),
_socket(client->get_io_service())
{
	_outq.add_producer(client->read_pauser());
	_outq.set_consumer(this);
}

fport_connection::~fport_connection()
{
	if(auto client = _client.lock())
		_outq.remove_producer(client->read_pauser());
	_outq.set_consumer();
}

void fport_connection::pqc_next(const shared_ptr<packet>& pkt)
{
	auto me = shared_from_this();
	async_write(
		_socket,
		pkt->payload,
		[this, pkt, me](const error_code& ec, const size_t)
		{
			if(ec)
				shutdown(me);
			_outq.pop();
		}
	);
}

void fport_connection::pqc_closed()
{
	auto me = shared_from_this();
	if(auto client = _client.lock())
		client->get_io_service().post(
			[this, me]()
			{
				shutdown(me);
			}
		);
}

void fport_connection::action()
{
	if(auto client = _client.lock())
		client->get_io_service().post(
			[this]()
			{
				read();
			}
		);
}

void fport_connection::queue_closed()
{
	auto me = shared_from_this();
	if(auto client = _client.lock())
		client->get_io_service().post(
			[me]()
			{
				me->shutdown(me);
			}
		);
}

void fport_connection::open(const string& remote_data)
{
	client_channel::open("fport", remote_data);
}

void fport_connection::handle_packet(const shared_ptr<packet>& pkt)
{
	_outq.push(pkt);
}

void fport_connection::state_changed(channel_state from, channel_state to)
{
	auto client = _client.lock();
	if(is_starting(from, to))
	{
		_me = shared_from_this();
		if(client)
		client->add_producer(this);
	}

	if(to == ACTIVE)
		trigger();
	if(is_stopping(from, to))
	{
		_socket.close();
		if(client)
		{
			client->remove_producer(this);
			client->get_io_service().post([this](){_me.reset();});
		}
	}
	client_channel::state_changed(from, to);
}

void fport_connection::read()
{
	auto me = _me;
	shared_ptr<packet> pkt = make_shared<packet>(_id);
	_socket.async_receive(
		pkt->payload.prepare(READ_SIZE),
		[this, pkt, me](const error_code& ec, const size_t sz)
		{
			if(ec)
			{
				if(ec != misc_errors::eof && ec != errc::operation_canceled)
					cerr << "error reading from forwarded port: " << ec.message() << endl;
				shutdown(me);
			}
			else
			{
				pkt->payload.commit(sz);
				if(auto client = _client.lock())
					client->write_packet(pkt);
				trigger();
			}
		}
	);
}
