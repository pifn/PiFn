#ifndef _CLIENT_SELECTOR_H
#define _CLIENT_SELECTOR_H

#include "action.h"
#include "client.h"
#include "pqueue.h"
#include "selector.h"

#include <stdio.h>

class queue_token : public pifn::pproducer
{
private:
	token _token;

public:
	queue_token(token t) :_token(t) {};

	virtual void pause() {}
	virtual void resume() {}
	virtual void queue_closed() {_token.revoke(); }
};

class fd_channel :
	public client_channel,
	private pifn::pconsumer<pifn::packet>,
	private pifn::pauseable_action
{
private:
	selector& _selector;
	queue_token _qtoken;
	const int _in_fd, _out_fd;
	pifn::pqueue _outq;
	const bool _remote_wait;

public:
	fd_channel(selector& selector, const std::shared_ptr<client>& client, const std::string& channel_type, const std::string& channel_data, int in_fd, int out_fd, bool remote_wait);
	virtual ~fd_channel();
	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);

	virtual void pqc_next(const std::shared_ptr<pifn::packet>& element);
	virtual void pqc_closed();

protected:
	virtual void state_changed(channel_state old_state, channel_state new_state);

private:
	virtual void action();
	void read();
	void write();
	void perror(const char*);
};

#endif
