#include "channel.h"
#include "config.h"
#include "config_dir.h"
#include "fd_file.h"
#include "option_defs.h"
#include "options.h"
#include "translator.h"
#include "util.h"

#include <openssl/ssl.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <vector>

#include <boost/assign.hpp>
#include <boost/process.hpp>
#include <boost/filesystem/exception.hpp>

using namespace boost::filesystem;
using namespace std;
namespace ps = boost::process;

class process_translator : public translator
{
public:
	process_translator(const process_translator&) = delete;
	void operator=(const process_translator&) = delete;

	process_translator(const string& command, const path& wd) :
		_proc(ps::launch_shell(command, make_context(wd))),
		_fd(_proc.get_stdout().handle().release())
	{
	}

	virtual ~process_translator()
	{
	}

	int input_stream()
	{
		return _fd;
	}

	void stop()
	{
		try {
			_proc.terminate();
			ps::status status = _proc.wait();
			int st = status.exited() ? status.exit_status() : -1;

			if(st)
			{
				cerr << "translator exited";
				if(st > 0)
					cerr << " with code " << st;
				cerr << endl;
			}
		}
		catch(exception& e)
		{
			cerr << "translator in unknown state: " << e.what() << endl;
			return;
		}
	}

private:
	static ps::context make_context(const path& wd)
	{
		ps::context ctx;
		ctx.work_directory = wd.native();
		ctx.environment = ps::self::get_environment();
		ctx.stdin_behavior = ps::inherit_stream();
		ctx.stdout_behavior = ps::capture_stream();
		ctx.stderr_behavior = ps::inherit_stream();
		return ctx;
	}

	ps::child _proc;
	const int _fd;
};

static string whoami()
{
	register struct passwd *pw;
	register uid_t uid;

	uid = geteuid();
	pw = getpwuid(uid);
	if(pw)
		return string(pw->pw_name);
	return string("");
}

static string whatami()
{
	char name[HOST_NAME_MAX];
	int rt = gethostname(name, HOST_NAME_MAX);
	if(!rt)
		return string(name);
	return string("");
}


static void fork_run(const string& process, const vector<string>& args)
{
	ps::context ctx;
	ctx.environment = ps::self::get_environment();
	ctx.stdout_behavior = ps::inherit_stream();
	ctx.stderr_behavior = ps::inherit_stream();
	ps::child c = ps::launch(process, args, ctx);
	ps::status status = c.wait();
	if(!status.exited())
		throw runtime_error(MKSTR("abnormal exit: " << process));
	if(status.exit_status())
		throw runtime_error(MKSTR("exit code " << status.exit_status() << ": " << process));
}

static void generate_key(const path& key_path)
{
	vector<string>  args = boost::assign::list_of("genrsa")("-out")(key_path.c_str())(KEY_SIZE);
	fork_run(OPENSSL_PATH, args);
}

static void generate_csr(const path& key_path, const path& csr_path, const path& openssl_config)
{
	string subject("/C=ZZ/ST=Unspecified/L=Unspecified/O=Pi Functions/OU=pifn/CN=");
	subject += whoami();
	subject += "@";
	subject += whatami();

	vector<string>  args = boost::assign::list_of
		("req")
		("-config") (openssl_config.c_str())
		("-sha256")
		("-new")
		("-key") (key_path.c_str())
		("-out") (csr_path.c_str())
		("-subj") (subject.c_str());
	fork_run(OPENSSL_PATH, args);
}

config_dir::config_dir() :
_ssl_context(boost::asio::ssl::context::sslv23)
{
}

config_dir::~config_dir()
{
}

bool config_dir::init(int argc, char* argv[])
{
	_options.parse_command_line(argc, argv);

	if(_options.show_help())
		return true;

	//get specified config file
	path config(_options.get<path>(OPT_CONFIG));
	if(is_directory(config))
	{
		//If it's a directory use default config file name
		_base_dir = config;
		config /= CONFIG_FILE_NAME;
	}
	else
		_base_dir = config.parent_path();

	if(config.native() != OPT_CONFIG_NONE)
	{
		//Parse local config file (pifn-local.conf) if it exists
		path lconfig = config.parent_path() / (
			config.stem().string() + CONFIG_LOCAL_SUFFIX + config.extension().string());
		if(exists(lconfig))
			_options.parse_file(lconfig);

		//Abort if main config file does not exist
		if(!exists(config))
			throw runtime_error(MKSTR("config file not found: " << config));
		_options.parse_file(config);
	}

	init_key();

	if(_options.has(OPT_RCRT) && !_options.get<path>(OPT_RCRT).empty())
		_remote_cert.reset(new certificate(resolve_option(OPT_RCRT)));

	if(_options.has(OPT_NOCONNECT))
		return true;

	const path& crt(resolve_option(OPT_CRT));
	if(!exists(crt))
	{
		const path& csr(resolve_option(OPT_CSR));
		cerr << endl
			<< "You cannot connect to " << connect_host() << " as you do not have a certificate yet. Please" << endl
			<< "send " << csr << " to the administrator of " << connect_host() << " and" << endl
			<< "in return they will give you a certificate and you should save this" << endl
			<< "to " << crt << "." << endl;
		if(connect_host() == "localhost")
			cerr << "As this is localhost, if you are the administrator you can run" << endl
				<< endl
				<< "\tsudo pifn-sign " << csr << " " << crt << endl
				<< endl;
		return true;
	}

	if(channel_type().size() > pifn::channel_action::TYPE_SIZE)
		throw out_of_range(MKSTR("channel type name too long: " << channel_type()));

	_ssl_context.use_private_key_file(key_file(), boost::asio::ssl::context::pem);

	certificate local_cert(crt);
	if(int time_validity = local_cert.validate_time())
	{
		const char* message = (time_validity > 0 ? "has expired" : "is not yet valid (maybe your clock is wrong?)");
		cerr << "WARNING: local certificate is not valid: it " << message << ": " << crt << endl;
	}
	if(1 != SSL_CTX_use_certificate(_ssl_context.native_handle(), local_cert.native_handle()))
		throw invalid_argument(MKSTR("openssl rejected " << crt));

	return false;
}

void config_dir::init_key()
{
	const path key_path(resolve_option(OPT_KEY));
	const path csr_path(resolve_option(OPT_CSR));

	bool nocreate = _options.has(OPT_NOCREATE);

	bool key_generated = false;
	if(!exists(key_path))
	{
		if(nocreate)
			throw runtime_error(MKSTR("refusing to create key: " << key_path));
		generate_key(key_path);
		key_generated = true;
	}

	if(key_generated || !exists(csr_path))
	{
		if(nocreate)
			throw runtime_error(MKSTR("refusing to create csr: " << key_path));
		generate_csr(key_path, csr_path, openssl_config());
	}
}

const path config_dir::resolve_option(const std::string& option_name) const
{
	path rel(_options.get<path>(option_name));
	if(rel.is_absolute())
		return rel;
	return _base_dir / rel;
}

shared_ptr<translator> config_dir::start_translator()
{
	if(!_options.has(OPT_TRANSLATOR) || !_options.get<string>(OPT_TRANSLATOR).length())
		return make_shared<translator>();

	return make_shared<process_translator>(_options.get<string>(OPT_TRANSLATOR), _base_dir);
}

string config_dir::key_file() const
{
	return resolve_option(OPT_KEY).native();
}

string config_dir::certificate_file() const
{
	return resolve_option(OPT_CRT).native();
}

path config_dir::openssl_config() const
{
	return path(_options.get<path>(OPT_OPENSSLCONF));
}

string config_dir::connect_host() const
{
	return _options.get<string>(OPT_HOST);
}

string config_dir::connect_port() const
{
	return _options.get<string>(OPT_PORT);
}

string config_dir::channel_type() const
{
	string full = _options.get<string>(OPT_CHANNEL);
	size_t so = full.find(OPT_CHANNEL_SEPARATER);
	return so == string::npos ? full : full.substr(0, so);
}

string config_dir::channel_data() const
{
	string full = _options.get<string>(OPT_CHANNEL);
	size_t so = full.find(OPT_CHANNEL_SEPARATER);
	return so == string::npos ? "" : full.substr(so + 1);
}

unique_ptr<fd_file> config_dir::output_file() const
{
	unique_ptr<fd_file> file;

	if(!_options.has(OPT_OUTPUT))
		file.reset(new fd_file(STDOUT_FILENO));
	else
		file.reset(new path_file(
			_options.get<string>(OPT_OUTPUT),
			O_CLOEXEC | O_WRONLY | O_TRUNC | O_CREAT,
			0666
		));

	return move(file);
}

bool config_dir::remote_wait() const
{
	return _options.has(OPT_WAIT);
}

vector<string> config_dir::fport_data() const
{
	if(_options.has(OPT_FPORT))
		return _options.get<vector<string>>(OPT_FPORT);
	return vector<string>();
}

const unique_ptr<const certificate>& config_dir::remote_certificate() const
{
	return _remote_cert;
}
