#include "token.h"

#include <cassert>

token_counter::token_counter(zero_func func) :
_count(0),
_on_zero(func)
{
	assert(func);
}

token_counter::~token_counter()
{
	assert(!_count);
}

token::token(const token& other) :
_counter(other._counter)
{
	increment();
}

token::token(token&& other) :
_counter(other._counter)
{
	other._counter = NULL;
}

token::token(token_counter& counter) :
_counter(&counter)
{
	increment();
}

token::~token()
{
	decrement(_counter);
}

token& token::operator=(const token& other)
{
	token_counter* last = _counter;
	_counter = other._counter;
	increment();
	decrement(last);

	return *this;
}

void token::increment()
{
	++_counter->_count;
}

void token::decrement(token_counter*& counter)
{
	if(counter != NULL)
	{
		int c = --counter->_count;
		assert(c >= 0);
		if(!c)
			counter->_on_zero();
		counter = NULL;
	}
}
