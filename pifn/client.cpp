#include <iostream>

#include "client.h"
#include "protocol.h"
#include "util.h"

#include <stdexcept>

using namespace pifn;
using namespace boost::asio;
using boost::lock_guard;
using boost::mutex;
using std::cerr;
using std::endl;
using std::exception;
using std::runtime_error;
using std::shared_ptr;
using std::string;

client_channel::client_channel(const shared_ptr<client>& client) :
_id(client->add_channel(this)),
_client(client),
_ch_state(INIT)
{
}

client_channel::client_channel(const shared_ptr<client>& client, int id) :
_id(id),
_client(client),
_ch_state(INIT)
{
}

client_channel::~client_channel()
{
	if(_id != 0)
	{
		if(auto client = _client.lock())
			client->remove_channel(this);
	}
}

void client_channel::open(const string& type, const string& data)
{
	set_state(OPENING);
	//Request server create this channel
	if(_id != 0)
	{
		channel_action cr(CR_ACTION_CREATE, _id, type, data);
		if(auto client = _client.lock())
			client->write_packet(cr, 0);
	}
	else
		assert(type.empty());
}

bool client_channel::is_running()
{
	lock_guard<mutex> guard(_lock);
	return is_running(_ch_state);
}

void client_channel::shutdown(shared_ptr<client_channel> keep)
{
	assert(_id != 0);
	if(auto client = _client.lock())
		client->get_io_service().post(
			[this, keep]()
			{
				channel_state old_state = set_state(CLOSING);

				if(old_state == OPENING || old_state == ACTIVE)
				{
					channel_action req(CR_ACTION_CLOSE, _id);
					auto pk = make_shared<packet>(0);
					streambuf_render(req, pk->payload);
					if(auto client = _client.lock())
						client->write_packet(pk);
				}
			}
		);
}

channel_state client_channel::set_state(channel_state to)
{
	channel_state from;
	{
		lock_guard<mutex> guard(_lock);
		if(_ch_state >= to) //Dont set state backwards
			return _ch_state;

		from = _ch_state;
		_ch_state = to;
	}

	//Passing current value of _ch_state so that the lock need not be aquired again
	state_changed(from, to);

	return from;
}

channel_state client_channel::set_state(channel_state from, channel_state to)
{
	{
		lock_guard<mutex> guard(_lock);
		if(_ch_state != from)
			return _ch_state;

		_ch_state = to;
	}

	//Passing current state so that the lock need not be aquired again
	state_changed(from, to);

	return from;
}

void client_channel::state_changed(channel_state old_state, channel_state new_state)
{
	assert(new_state > old_state);
	if(!is_running(new_state))
	{
		if(auto client = _client.lock())
			client->remove_channel(this);
	}
}

client_channel_manager::client_channel_manager(const shared_ptr<client>& client) :
client_channel(client, 0)
{
	assert(_id == 0);
	open("");
}

void client_channel_manager::handle_packet(const shared_ptr<packet>& packet)
{
	auto client = _client.lock();
	if(!client)
		return;
	channel_action cr;
	streambuf_parse(cr, packet->payload);

	client_channel* channel = client->get_channel(cr.target);
	if(channel == NULL)
	{
		if(cr.action == CR_ACTION_CLOSE)
			return; //closing a non-existant channel is acceptable
		cerr << "command for unused channel " << cr.target << endl;
		client->shutdown();
		return;
	}

	switch(cr.action)
	{
		case CR_ACTION_CREATE:
			channel->set_state(OPENING, ACTIVE);
			break;
		case CR_ACTION_CREATE | CR_ACTION_FAIL:
			cerr << "channel " << cr.target << " not created";
			if(!cr.type_data.empty())
				cerr << ": " << cr.type_data;
			cerr << endl;
			channel->set_state(FAILED);
			break;
		case CR_ACTION_CLOSE:
			channel->set_state(CLOSED);
			break;
		default:
			cerr << "unknown action number " << (int)cr.action << endl;
			assert(false);
			client->shutdown();
	}
}

client::client(io_service& ios, ssl::context& context, const certificate* remote_cert) :
async_stream(ios, context, 0),
_next_channel(0),
_ssl_active(false),
_remote_cert(remote_cert),
_manual_cert_check(false)
{
	_socket.set_verify_mode(ssl::verify_peer);
	_socket.set_verify_callback(
		boost::bind(&client::verify_certificate, this, _1, _2)
	);
}

bool client::verify_certificate(bool /*preverified*/, ssl::verify_context& ctx) {
	certificate sent_cert(X509_STORE_CTX_get_current_cert(ctx.native_handle()), false);
	if(_remote_cert)
	{
		if(sent_cert == *_remote_cert)
			return true;

		cerr << "WARNING: remote server certificate does not match local copy." << endl
			<< "WARNING: it might not be who you think it is." << endl
			<< "expecting: " << _remote_cert->subject_one_line() << endl
			<< "received:  " << sent_cert.subject_one_line() << endl;
		return false;
	}

	if(!_manual_cert_check)
	{
		_manual_cert_check = true;
		cerr << "WARNING: no local copy to check server certificate against." << endl
			<< "WARNING: please check manually" << endl;
	}
	cerr << "server sent certificate: " << sent_cert.subject_one_line() << endl
		<< "  SHA1 Fingerprint: " << sent_cert.sha1_string() << endl;

	return true; //Allow connection
}

void client::connect(ip::tcp::resolver::iterator& endpoint_iterator)
{
	assert(_channels.count(0)); //add_channel0 needs to have been called by now
	boost::asio::connect(_socket.lowest_layer(), endpoint_iterator);
	_socket.handshake(ssl::stream_base::client);
	_ssl_active = true;
}

void client::welcome()
{
	shared_ptr<streambuf> out_buffer = make_shared<streambuf>(INITIAL_TEXT_LENGTH);
	{
		std::ostream sout(out_buffer.get());
		serialiser sr(sout);
		sr.write_string("p1 " IMPLEMENTATION_NAME, INITIAL_TEXT_LENGTH);
	}
	async_write(_socket, *out_buffer, [this, out_buffer](const boost::system::error_code& ec, const size_t txsz)
	{
		(void)txsz;
		if(ec)
			shutdown();
	});

	streambuf in_buffer(INITIAL_TEXT_LENGTH);
	boost::system::error_code ec;
	read(_socket, in_buffer, ec);
	if(in_buffer.size() != INITIAL_TEXT_LENGTH || ec)
	{
		io_error("reading welcome text", ec);
		return;
	}
	//TODO: log welcome text from in_buffer e.g.
	/*
	 * std::istream ins(&in_buffer);
	 * deserialiser de(ins);
	 * string str = de.read_string(INITIAL_TEXT_LENGTH);
	 * cerr << "server implementation: '" << str << "'" << endl;
	*/
}

void client::shutdown()
{
	if(_ssl_active) {
		_ssl_active = false;
		_socket.async_shutdown([this](const boost::system::error_code& ec) {
			if(ec)
				show_error("SSL shutdown", ec);
			close();
		});
	}
}

void client::close()
{
	auto& soc = _socket.lowest_layer();
	boost::system::error_code ec;

	soc.shutdown(ip::tcp::socket::shutdown_type::shutdown_both, ec);
	if(ec)
		cerr << "socket shutdown failed: " << ec << " - " << ec.message() << endl;
	soc.close();
	get_io_service().stop();
}

void client::handle_packet(const shared_ptr<packet>& packet)
{
	try
	{
		auto ch_it = _channels.find(packet->channel);
		if(ch_it == _channels.end())
			throw runtime_error(MKSTR("unknown channel: " << packet->channel));
		ch_it->second->handle_packet(packet);
	}
	catch(exception& e)
	{
		cerr << "cannot handle packet: " << e.what() << endl;
		shutdown();
	}
}

void client::io_error(const std::string& where, const boost::system::error_code& ec)
{
	show_error(where, ec);
	shutdown();
}

void client::show_error(const std::string& message, const boost::system::error_code& ec)
{
	if(
		ec == boost::system::errc::operation_canceled
		|| ec == error::misc_errors::eof
	)
		return;

	if(
		ec.category() == error::get_ssl_category()
		&& ERR_GET_LIB(ec.value()) == ERR_R_SSL_LIB
		&& ERR_GET_REASON(ec.value()) == SSL_R_SHORT_READ
	)
		return;

	cerr << message << ": " << ec << " - " << ec.message() << endl;
}

uint16_t client::add_channel(client_channel* channel)
{
	uint16_t id = free_channel_number();
	_channels[id] = channel;
	return id;
}

void client::add_channel0() {
	assert(_manager.get() == NULL);
	_manager.reset(new client_channel_manager(shared_from_this()));
	_channels[0] = _manager.get();
}

client_channel* client::remove_channel(uint16_t ch_num)
{
	assert(ch_num != 0);

	auto ch_it = _channels.find(ch_num);
	assert(ch_it != _channels.end());
	client_channel* channel = ch_it->second;
	_channels.erase(ch_it);

	if(_channels.size() == 1)
		close_writeq();

	return channel;
}

bool client::remove_channel(client_channel* channel)
{
	auto ch_it = _channels.find(channel->id());
	if(ch_it == _channels.end() || ch_it->second != channel)
		return false;

	_channels.erase(ch_it);

	assert(_channels.size() >= 1);
	if(_channels.size() == 1)
		close_writeq();

	return true;
}

void client::remove_all_channels()
{
	for(auto it = _channels.begin(); it != _channels.end(); ++it)
	{
		if(it->first == 0)
			continue;

		client_channel* channel = it->second;
		it = _channels.erase(it);

		if(channel->get_state() != FAILED)
			channel->set_state(FAILED);
	}
}

client_channel* client::get_channel(uint16_t number)
{
	auto ch_it = _channels.find(number);
	if(ch_it == _channels.end())
		return NULL;
	return ch_it->second;
}

uint16_t client::free_channel_number()
{
	while(_channels.count(_next_channel))
		++_next_channel;
	return _next_channel;
}
