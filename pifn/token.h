#ifndef _TOKENS_H
#define _TOKENS_H

#include <atomic>
#include <boost/utility.hpp>
#include <functional>

class token;

class token_counter : boost::noncopyable
{
	friend class token;

public:
	typedef std::function<void()> zero_func;
private:
	std::atomic_int _count;
	zero_func _on_zero;

public:

	token_counter(zero_func on_zero);
	virtual ~token_counter();
};

class token
{
private:
	token_counter* _counter;

public:
	token(const token& other);
	token(token&& other);
	token(token_counter& counter);
	virtual ~token();

	token& operator=(const token& other);
	inline void revoke() {decrement(_counter); }
	inline bool valid() {return _counter != NULL; }

private:
	void increment();
	static void decrement(token_counter*& counter);
};

#endif
