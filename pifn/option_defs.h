#ifndef _OPTION_DEFS_H
#define _OPTION_DEFS_H

#include "common_option_defs.h"

#define OPT_HOST "host"
#define DEC_HOST OPT_HOST ",h"
#define DEF_HOST "localhost"

#define OPT_PORT "port"
#define DEC_PORT OPT_PORT ",p"
#define DEF_PORT "3497"

#define OPT_CONFIG "configfile"
#define DEC_CONFIG OPT_CONFIG
#define DEF_CONFIG "."
#define OPT_CONFIG_NONE ":"

#define OPT_KEY "key"
#define DEC_KEY OPT_KEY ",k"
#define DEF_KEY "local.key"

#define OPT_CSR "csr"
#define DEC_CSR OPT_CSR
#define DEF_CSR "local.csr"

#define OPT_CRT "certificate"
#define DEC_CRT OPT_CRT ",c"
#define DEF_CRT "local.pem"

#define OPT_RCRT "remote-certificate"
#define DEC_RCRT OPT_RCRT

#define OPT_OPENSSLCONF "openssl-conf"
#define DEC_OPENSSLCONF OPT_OPENSSLCONF
#define DEF_OPENSSLCONF CONF_DIR "/openssl.conf"

#define OPT_NOCREATE "no-create"
#define DEC_NOCREATE OPT_NOCREATE ",n"

#define OPT_NOCONNECT "no-connect"
#define DEC_NOCONNECT OPT_NOCONNECT

#define OPT_TRANSLATOR "translator"
#define DEC_TRANSLATOR OPT_TRANSLATOR ",t"

#define OPT_CHANNEL "channel"
#define DEC_CHANNEL OPT_CHANNEL
#define DEF_CHANNEL "shell"
#define OPT_CHANNEL_SEPARATER ':'

#define OPT_OUTPUT "output"
#define DEC_OUTPUT OPT_OUTPUT ",o"

#define OPT_WAIT "remote-wait"
#define DEC_WAIT OPT_WAIT ",w"

#define OPT_FPORT "forward-port"
#define DEC_FPORT OPT_FPORT ",L"

#endif
