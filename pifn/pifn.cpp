#include "client.h"
#include "config_dir.h"
#include "fport.h"
#include "selector.h"
#include "client_selector.h"

#include <iostream>
#include <memory>
#include <vector>

using namespace std;
using boost::asio::io_service;

class io_service_thread
{
private:
	io_service& _ios;
	boost::thread _ios_thread;
	std::unique_ptr<io_service::work> _work;

public:
	io_service_thread(io_service& ios, selector& selector, client& client) :
	_ios(ios),
	_ios_thread(
		[&ios, &selector, &client]()
		{
			token t = selector.work();
			ios.run();
			client.remove_all_channels();
		}
	),
	_work(new io_service::work(_ios))
	{
	}

	virtual ~io_service_thread()
	{
		stop();
		if(_ios_thread.joinable())
			_ios_thread.join();
	}

	void stop()
	{
		_work.reset();
	}
};

class pifn_connector
{
private:
	unique_ptr<fd_file> _output;
	selector& _selector;
	const shared_ptr<client> _client;
	config_dir& _config;
	unique_ptr<fd_channel> _channel;
	shared_ptr<translator> _tr;
	vector<unique_ptr<fport_listener>> _fports;

public:
	pifn_connector(selector& selector, const shared_ptr<client>& client, config_dir& config) :
	_selector(selector),
	_client(client),
	_config(config)
	{
		vector<string> fpds = config.fport_data();
		for(string line : fpds)
			_fports.push_back(unique_ptr<fport_listener>(new fport_listener(client, line)));
	}

	void connect()
	{
		_output = _config.output_file();
		_tr = _config.start_translator();

		_client->welcome();
		cerr << "Connected" << endl;
		_channel.reset(new fd_channel(
			_selector,
			_client,
			_config.channel_type(),
			_config.channel_data(),
			_tr->input_stream(),
			_output->fd(),
			_config.remote_wait()
		));
		_client->start_read();
	}
};

int main(int argc, char* argv[]) {
	io_service ios;
	selector sel;
	config_dir conf;

	try {
		if(conf.init(argc, argv))
			return EXIT_SUCCESS;
	}
	catch(exception& e)
	{
		cerr << "Exception: " << e.what() << endl;
		return EXIT_FAILURE;
	}

	const auto con = make_shared<client>(ios, conf.ssl_context(), conf.remote_certificate().get());
	con->add_channel0();

	int rt = EXIT_SUCCESS;
	try {
		const string host = conf.connect_host();
		const string port = conf.connect_port();
		const string channel_type = conf.channel_type();
		cerr << "Connecting to: " << host << ":" << port << endl;

		boost::asio::ip::tcp::resolver resolver(ios);
		boost::asio::ip::tcp::resolver::query query(host, port);
		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

		con->connect(iterator);
		pifn_connector connector(sel, con, conf);

		ios.post(
			[&connector]()
			{
				connector.connect();
			}
		);
		io_service_thread iost(ios, sel, *con);
		sel.run();
	} catch(exception& e) {
		cerr << "Exception: " << e.what() << endl;
		rt = EXIT_FAILURE;
	}

	return rt;
}
