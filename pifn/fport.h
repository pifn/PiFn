#ifndef _FPORT_H
#define _FPORT_H

#include "action.h"
#include "client.h"
#include "selector.h"

#include <boost/asio.hpp>
#include <boost/utility.hpp>
#include <memory>
#include <string>
#include <unordered_set>

class fport_connection;

class fport_listener :
	pifn::pauseable_action,
	boost::noncopyable
{
private:
	std::unordered_set<std::shared_ptr<fport_connection>> _connections;
	const std::weak_ptr<client> _client;
	boost::asio::ip::tcp::acceptor _acceptor;
	std::string _remote_data;

public:
	fport_listener(const std::shared_ptr<client>&, const std::string&);
	virtual ~fport_listener();

private:
	void accept();
	virtual void action();
	virtual void queue_closed();
};

class fport_connection :
	public client_channel,
	public std::enable_shared_from_this<fport_connection>,
	private pifn::pconsumer<pifn::packet>,
	pifn::pauseable_action
{
	friend class fport_listener;

private:
	std::shared_ptr<fport_connection> _me;
	boost::asio::ip::tcp::socket _socket;
	pifn::pqueue _outq;

public:
	fport_connection(const std::shared_ptr<client>&);
	virtual ~fport_connection();

	void open(const std::string& remote_data);
	virtual void handle_packet(const std::shared_ptr<pifn::packet>&);
	virtual void state_changed(channel_state from, channel_state to);
	virtual void pqc_next(const std::shared_ptr<pifn::packet>& element);
	virtual void pqc_closed();

private:
	void read();
	virtual void action();
	virtual void queue_closed();
};

#endif
