#ifndef _SELECTOR_H
#define _SELECTOR_H

#include <token.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <exception>
#include <iostream>
#include <map>
#include <sys/select.h>

#define INTR_NONE     0
#define INTR_READ     1
#define INTR_WRITE    2
#define INTR_BOTH     3
#define INTR_CANCELED 4

typedef std::function<void(int)> select_handler;

class fdpipe {
public:
	fdpipe(bool close_on_exec = true);
	~fdpipe();

	inline int read() {return _fds[0];};
	inline int write() {return _fds[1];};

	void close();

private:
	int _fds[2];
};

struct tracker
{
	int interest;
	select_handler handler;

	tracker() : interest(0) {}
};

class selector {
private:
	typedef std::map<int, tracker> fd_map;

	class signaller
	{
	private:
		fdpipe _sigpipe;
		selector& _selector;

	public:
		signaller(selector&);
		void signal(char);

	private:
		void handler(int);
	};

	boost::mutex _lock;
	bool _stopped;

	fd_map _handlers;
	signaller _signaller;
	token_counter _counter;

public:
	selector(const selector&) = delete;
	const selector& operator=(const selector&) = delete;

	selector();
	virtual ~selector() {};

	void track(int fd, select_handler handler);
	void track(int fd, int interest, bool interrupt = true);
	void untrack(int fd);

	token work();
	void run();

private:
	void notify(int fd, int interest);
	bool stopped();
};

#endif
