#include "client_selector.h"
#include "util.h"

#include <boost/bind.hpp>
#include <string.h>

#define READ_SIZE 4096

using namespace pifn;
using boost::asio::buffer_cast;
using boost::asio::io_service;
using boost::bind;
using boost::lock_guard;
using boost::mutex;
using boost::system::error_code;
using std::shared_ptr;

fd_channel::fd_channel(
	selector& selector,
	const shared_ptr<client>& client,
	const std::string& channel_type,
	const std::string& channel_data,
	int in_fd,
	int out_fd,
	bool remote_wait
) :
client_channel(client),
_selector(selector),
_qtoken(selector.work()),
_in_fd(in_fd),
_out_fd(out_fd),
_outq(),
_remote_wait(remote_wait)
{
	open(channel_type, channel_data);
	_outq.add_producer(&_qtoken);
	_selector.track(
		_in_fd,
		[this](int interest)
		{
			assert((interest & ~INTR_CANCELED) == INTR_READ);
			if(interest == INTR_READ)
				read();
		}
	);
	_selector.track(
		_out_fd,
		[this](int interest)
		{
			assert((interest & ~INTR_CANCELED) == INTR_WRITE);
			if(interest == INTR_WRITE)
				write();
		}
	);
	client->add_producer(this);
	_outq.add_producer(client->read_pauser());
	_outq.set_consumer(this);
}

fd_channel::~fd_channel()
{
	_selector.untrack(_in_fd);
	_selector.untrack(_out_fd);
	_outq.set_consumer();
	if(auto client = _client.lock())
	{
		client->remove_producer(this);
		_outq.remove_producer(client->read_pauser());
	}
}

void fd_channel::pqc_next(const std::shared_ptr<pifn::packet>& /*packet*/)
{
	_selector.track(_out_fd, INTR_WRITE);
}

void fd_channel::pqc_closed()
{
}

void fd_channel::action()
{
	_selector.track(_in_fd, INTR_READ);
}

void fd_channel::read()
{
	shared_ptr<packet> p = make_shared<packet>(_id);
	ssize_t rt = ::read(_in_fd, buffer_cast<char*>(p->payload.prepare(READ_SIZE)), READ_SIZE);
	auto client = _client.lock();
	if(!client)
		return;

	if(rt > 0)
	{
		p->payload.commit(rt);
		client->write_packet(p);
		trigger();
	}
	else if(rt == 0 || errno != EINTR)
	{
		assert(rt <= 0);
		if(rt != 0)
			perror("read");
		if(!_remote_wait)
			shutdown();
	}
	else
		trigger();
}

void fd_channel::handle_packet(const std::shared_ptr<pifn::packet>& packet)
{
	_outq.push(packet);
}

void fd_channel::write()
{
	shared_ptr<packet> packet;
	bool closed = _outq.front(packet);
	if(closed || !packet)
		return;

	ssize_t rt = ::write(
		_out_fd,
		buffer_cast<const char*>(packet->payload.data()),
		packet->payload.size()
	);

	if(rt <= 0)
	{
		if(rt < 0 && errno != EAGAIN)
		{
			perror("write");
			_outq.close_front();
		}
		else
			write();
	}
	else
	{
		packet->payload.consume(rt);
		_outq.pop();
	}
}

void fd_channel::perror(const char* message)
{
	char errb[100];
	const char* err_str = strerror_r(errno, errb, 100);
	std::cerr << message << ": " << err_str << std::endl;
}

void fd_channel::state_changed(channel_state old_state, channel_state new_state)
{
	client_channel::state_changed(old_state, new_state);
	if(new_state == ACTIVE)
		trigger();
	if(new_state == CLOSED || new_state == FAILED)
	{
		if(auto client = _client.lock())
			client->close_writeq(); //Ensure that closing this channel closes all channels
		_outq.close_back();
	}
}
