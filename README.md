# Pi Functions

This is a project to control hardware attached to a Raspberry Pi from across a network with a focus on flexibility and ease of scripting.

## Overview

The two main parts of Pi Function are the ```pifn``` and ```pifnd``` programs. ```pifnd``` is the server that runs on the Raspberry Pi that has the hardware attached to it and it controls the hardware. ```pifn``` is a client that connects to the server and is used to interact with it. [Sir Lancelot](https://github.com/Xianic/Sir-Lancelot) is an example project.

[Further Documentation](doc/index.md)
