#include "hardware.h"

#define TOP 4096

const int PWM_STEPS[] =
{
	0,
	(int)(0.264 * TOP),
	(int)(0.397 * TOP),
	(int)(0.494 * TOP),
	(int)(0.632 * TOP),
	(int)(0.747 * TOP),
	(int)(0.874 * TOP),
	TOP
};

hardware::hardware(config& config) :
_pifn(config.i2c_bus().c_str(), PIFN_ADDR),
_servos(config.i2c_bus().c_str(), PCA9685_DEFAULT_ADDRESS)
{
	_pifn.initialize(1250);
	_pifn.setPWM(MT_NUM * 2, 0);
	_servos.initialize(50, 24576000);
	led(NAVIO_COLOR_ORANGE);
}

hardware::~hardware()
{
	_pifn.setPWM(MT_NUM * 2, 0);
	_pifn.sleep();
	_servos.setPWM(16, 0);
	led(NAVIO_COLOR_WHITE);
}

void hardware::drive(int motor, int speed)
{
	bool reverse = speed < 0;
	if(reverse)
		speed *= -1;

	int a, b;
	if(speed == 0)
	{
		a = TOP;
		b = TOP;
	}
	else
	{
		a = TOP;
		b = TOP - PWM_STEPS[speed];
	}

	if(reverse)
	{
		int c = a;
		a = b;
		b = c;
	}

	_pifn.setPWM(motor * 2, a);
	_pifn.setPWM(motor * 2 + 1, b);
}

void hardware::float_stop(int motor)
{
	_pifn.setPWM(motor * 2, 0);
	_pifn.setPWM(motor * 2 + 1, 0);
}

void hardware::led(float red, float green, float blue)
{
	_servos.setPWM(0, 4096.0 * (1.0 - blue));
	_servos.setPWM(1, 4096.0 * (1.0 - green));
	_servos.setPWM(2, 4096.0 * (1.0 - red));
}

void hardware::servo(int port, float position)
{
	if(port < 0 || port > 13)
		return;
	if(position > 1)
		position = 1;
	if(position < -1)
		position = -1;
	_servos.setPWMmS(port + 3, 1.7 + position);
}

void hardware::pwm(int port, float duty_cycle)
{
	if(port < 0 || port > 13)
		return;
	if(duty_cycle > 1)
		duty_cycle = 1;
	if(duty_cycle < 0)
		duty_cycle = 0;
	_servos.setPWM(port + 3, 4096.0 * duty_cycle);
}
