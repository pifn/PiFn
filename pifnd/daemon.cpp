#include "daemon.h"
#include "util.h"

#include <sstream>
#include <string>
#include <system_error>
#include <unistd.h>

daemon_pid::daemon_pid(config& config) :
_pfh(NULL)
{
	std::string pidfile(config.pidfile());

	if(!pidfile.empty())
	{
		pid_t other;
		_pfh = pidfile_open(pidfile.c_str(), 0644, &other);
		if(!_pfh)
		{
			if(errno == EEXIST)
				throw std::system_error(errno, std::system_category(), MKSTR("Already running with pid " << other));
			throw std::system_error(errno, std::system_category(), config.pidfile());
		}
	}
}

void daemon_pid::daemonise(bool daemonise)
{
	if(daemonise && daemon(0, 0))
	{
		if(_pfh)
			pidfile_remove(_pfh);
		throw std::system_error(errno, std::system_category(), std::string("Could not become a daemon"));
	}

	if(_pfh)
		pidfile_write(_pfh);
}

daemon_pid::~daemon_pid()
{
	if(_pfh)
		pidfile_remove(_pfh);
}
