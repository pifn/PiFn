#include "config.h"
#include "options.h"
#include "option_defs.h"
#include "util.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

options::options() :
_config("Config file options"),
_cmd_line("Command line options")
{
	_cmd_line.add_options()
		(DEC_HELP, "show help")
		(DEC_VERSION, "show version and exit")
		(DEC_CONFIG_FILE, po::value<fs::path>(), "config file")
		(DEC_DETACH, "detach from the terminal and become a daemon")
	;
	_config.add_options()
		(DEC_I2C_BUS, po::value<fs::path>()->default_value(DEF_I2C_BUS))
		(DEC_PIDFILE, po::value<fs::path>()->default_value(DEF_PIDFILE))
		(DEC_BIND_HOST, po::value<string>()->default_value(DEF_BIND_HOST))
		(DEC_BIND_PORT, po::value<string>()->default_value(DEF_BIND_PORT))
		(DEC_KEY_FILE, po::value<fs::path>()->default_value(DEF_KEY_FILE))
		(DEC_CRT_FILE, po::value<fs::path>()->default_value(DEF_CRT_FILE))
		(DEC_CA_CRT_FILE, po::value<fs::path>()->default_value(DEF_CA_CRT_FILE))
		(DEC_FPORT_MAP, po::value<vector<string>>())
		(DEC_FPORT_FILTER, po::value<vector<string>>())
	;
}

options::~options() {
}

void options::parse_command_line(int argc, char* argv[])
{
	po::store(po::parse_command_line(argc, argv, _cmd_line), _vm);
	po::notify(_vm);
}

void options::parse_file(const fs::path& file)
{
	ifstream ifs(file.c_str());
	if(!ifs)
		throw runtime_error(MKSTR("could not read file: " << file));

	store(parse_config_file(ifs, _config), _vm);
	notify(_vm);
}

void options::parse_file()
{
	const char* argv0 = "";
	po::store(po::parse_command_line(0, &argv0, _config), _vm);
	po::notify(_vm);
}

bool options::show_help()
{
	bool exit_soon = false;

	if(has(OPT_VERSION))
	{
		cout << "Version: " << PACKAGE_VERSION << endl;
		exit_soon = true;
	}

	if(has(OPT_HELP))
	{
		cout << "Usage:" << endl
			<< "\tpifnd [-d] [-c config file]" << endl
			<< endl
			<< _cmd_line << endl;
		exit_soon = true;
	}

	return exit_soon;
}

bool options::has(const string& name) const
{
	return _vm.count(name);
}
