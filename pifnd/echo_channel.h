#ifndef _ECHO_CHANNEL_H
#define _ECHO_CHANNEL_H

#include "server.h"

/*
 * A simple channel implementation that either echos back each packet it receives,
 * or eats it without trace.
 */
class echo_channel : public server_channel
{
private:
	size_t _bytes, _packets;
	const bool _echo;

public:
	echo_channel(bool echo, uint16_t channel_number, const std::shared_ptr<connection>& connection);
	virtual ~echo_channel();
	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);

	virtual void shutdown(const std::string& error = "");
};

class echo_registrator : public channel_registry
{
private:
	const bool _echo;

public:
	echo_registrator(const std::string& name, bool echo);

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&);
};

extern echo_registrator echo_registration;
extern echo_registrator null_registration;

#endif
