#ifndef _DAEMON_H
#define _DAEMON_H

#include <bsd/libutil.h>

#include "configfile.h"

class daemon_pid
{
public:
	daemon_pid(config& config);
	virtual ~daemon_pid();
	void daemonise(bool daemonise);

private:
	struct pidfh* _pfh;
};

#endif
