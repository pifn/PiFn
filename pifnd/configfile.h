#ifndef _CONFIG_H
#define _CONFIG_H

#include "options.h"

#include <string>
#include <vector>

class config
{
private:
	options _options;
	const std::vector<std::string> _empty;

public:
	config();
	int init(int argc, char* argv[]);

	bool daemonise() const;
	const std::string& i2c_bus() const;
	const std::string& bind_host() const;
	const std::string& bind_port() const;
	const std::string& pidfile() const;
	const std::string& key_file() const;
	const std::string& crt_file() const;
	const std::string& ca_crt_file() const;
	const std::vector<std::string>& fport_mapping() const;
	const std::vector<std::string>& fport_filters() const;

private:
	void parse_config_file(const char* file);
};

#endif
