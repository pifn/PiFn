#include "configfile.h"
#include "option_defs.h"
#include "util.h"

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
using namespace std;

config::config()
{
}

int config::init(int argc, char* argv[])
{
	_options.parse_command_line(argc, argv);

	if(_options.show_help())
		return true;

	if(_options.has(OPT_CONFIG_FILE))
		_options.parse_file(_options.get<fs::path>(OPT_CONFIG_FILE));
	else
		_options.parse_file();

	return false;
}

bool config::daemonise() const
{
	return _options.has(OPT_DETACH);
}

const string& config::i2c_bus() const
{
	return _options.get<fs::path>(OPT_I2C_BUS).native();
}

const string& config::bind_host() const
{
	return _options.get<string>(OPT_BIND_HOST);
}

const string& config::bind_port() const
{
	return _options.get<string>(OPT_BIND_PORT);
}

const string& config::pidfile() const
{
	return _options.get<fs::path>(OPT_PIDFILE).native();
}

const string& config::key_file() const
{
	return _options.get<fs::path>(OPT_KEY_FILE).native();
}

const string& config::crt_file() const
{
	return _options.get<fs::path>(OPT_CRT_FILE).native();
}

const string& config::ca_crt_file() const
{
	return _options.get<fs::path>(OPT_CA_CRT_FILE).native();
}

const vector<string>& config::fport_mapping() const
{
	if(_options.has(OPT_FPORT_MAP))
		return _options.get<vector<string>>(OPT_FPORT_MAP);
	return _empty;
}

const vector<string>& config::fport_filters() const
{
	if(_options.has(OPT_FPORT_FILTER))
		return _options.get<vector<string>>(OPT_FPORT_FILTER);
	return _empty;
}
