#ifndef _COMMAND_H
#define _COMMAND_H

#include "channel.h"
#include "configfile.h"
#include "hardware.h"
#include "server.h"

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <memory>
#include <iostream>
#include <sstream>

#define MT_TIMEOUT_STOP 1.5

class mt_timeout
{
private:
	boost::asio::deadline_timer _timer;
	hardware& _hw;
	const int _channel;
	bool _break_motor;

public:
	mt_timeout(const mt_timeout&) = delete;
	const mt_timeout& operator=(const mt_timeout&) = delete;

	mt_timeout(boost::asio::io_service& io_service, hardware& hw, int channel);
	virtual ~mt_timeout();

	void timeout(bool break_motor, float timeout);
	void timeout_for_stop() {timeout(false, MT_TIMEOUT_STOP); }
	inline void cancel() {_timer.cancel(); };

private:
	void timeout_expiered();
};

class shell_channel : public server_channel
{
private:
	std::stringstream _in;
	std::shared_ptr<pifn::packet> _out;
	std::ostream _sout;
	hardware& _hw;
	std::unique_ptr<mt_timeout> _timers[MT_NUM];

public:
	shell_channel& operator=(const shell_channel&) = delete;
	shell_channel(const shell_channel&) = delete;

	shell_channel(hardware& hw, const std::shared_ptr<connection>& connection, uint16_t channel_number);
	virtual ~shell_channel() {};

protected:
	void handle_packet(const std::shared_ptr<pifn::packet>& packet);

private:
	void parse_command();
	void drive();
	void servo();
	void pwm();
	void led();
	void echo();
	void help();

	int read_int(int min, int max);
	float read_float(float min, float max);
	float read_float(float min);
};

class shell_registrator : public channel_registry
{
private:
	hardware& _hw;

public:
	shell_registrator(hardware& hardware);
	virtual ~shell_registrator() {};

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&);
};

#endif
