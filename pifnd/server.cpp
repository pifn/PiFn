#include "server.h"

#include "channel.h"
#include "config.h"
#include "server_channel.h"
#include "protocol.h"
#include "util.h"

#include <stdexcept>
#include <string>

using namespace pifn;
using namespace boost::asio::ssl;
using boost::asio::buffer_cast;
using boost::asio::ip::tcp;
using boost::asio::streambuf;
using boost::system::error_code;
using std::cerr;
using std::endl;
using std::exception;
using std::make_shared;
using std::runtime_error;
using std::shared_ptr;
using std::string;

server_channel::server_channel(uint16_t channel_number0, const shared_ptr<connection>& con) :
_con(con),
channel_number(channel_number0)
{
}

void server_channel::create_notify(const string& error)
{
	channel_action action(CR_ACTION_CREATE, channel_number);
	if(!error.empty())
	{
		action.action |= CR_ACTION_FAIL;
		action.type_data = error;
	}
	auto con = _con.lock();
	if(con)
	{
		con->get_io_service().post(
			[con, action]()
			{
				con->write_packet(action, 0);
				if(action.action & CR_ACTION_FAIL)
					con->remove_channel(action.target);
			}
		);
	}
}

shared_ptr<packet> server_channel::make_packet()
{
	return make_shared<packet>(channel_number);
}

void server_channel::write_packet(const shared_ptr<packet>& packet)
{
	assert(channel_number == packet->channel);
	auto con = _con.lock();
	if(con)
		con->write_packet(packet);
}

void server_channel::shutdown(const string& error)
{
	channel_action resp(CR_ACTION_CLOSE, channel_number);
	resp.type_data = error;

	auto pk = make_shared<packet>(0);
	streambuf_render(resp, pk->payload);
	auto con = _con.lock();
	if(!con)
		return;

	con->write_packet(pk);
	const int ch_num = channel_number;
	con->get_io_service().post(
		[con, ch_num]() // Don't capture this
		{
			//Channel is destructed here
			con->remove_channel(ch_num);
		}
	);
}

connection::connection(server& server, uint32_t connection_number) :
async_stream(server.io_service(), server.ssl_context(), connection_number),
_server(server),
_stopped(false)
#ifndef NDEBUG
,_started(false)
#endif
{
	_socket.set_verify_mode(verify_peer | verify_fail_if_no_peer_cert);
	_socket.set_verify_callback([this](bool preverified, verify_context& ctx)
	{
		char subject_name[256];
		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
		X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
		log(MKSTR("verifying " << subject_name << ": preverified: " << preverified));
		return preverified;
	});
}

connection::~connection()
{
	for(auto c : _channels)
		c.second->connection_closed();
}

void connection::log(const string& message)
{
	cerr << _name << ": " << message << endl;
}

void connection::add_channel(shared_ptr<server_channel> channel)
{
	assert(!_channels.count(channel->channel_number));
	_channels[channel->channel_number] = channel;
}

void connection::add_channel0()
{
	channel_action action(CR_ACTION_CREATE, 0, "");
	auto me = shared_from_this(); //this cannot be done in the constructor
	add_channel(channel_registry::create(me, action));
}

void connection::remove_channel(uint16_t channel)
{
	assert(_channels.count(channel));
	_channels.erase(channel);
}

shared_ptr<server_channel> connection::get_channel(uint16_t number)
{
	auto ch_it = _channels.find(number);
	if(ch_it == _channels.end())
		return shared_ptr<server_channel>();
	return ch_it->second;
}

void connection::start()
{
#ifndef NDEBUG
	assert(!_started);
	_started = true;
#endif
	_name = MKSTR(lowest_socket().remote_endpoint());
	log("connected");
	_socket.async_handshake(
		stream_base::server,
		[this](error_code ec) {
			log(MKSTR("handshake complete: " << ec << " - " << ec.message()));
			if(ec)
				shutdown();
			else
				welcome();
		}
	);
}

void connection::welcome()
{
	shared_ptr<streambuf> out_buffer = make_shared<streambuf>();
	{
		std::ostream sout(out_buffer.get());
		serialiser sr(sout);
		sr.write_string("p1 " IMPLEMENTATION_NAME, INITIAL_TEXT_LENGTH);
	}
	async_write(_socket, *out_buffer, [this, out_buffer](const error_code& ec, const size_t txsz)
	{
		(void)txsz;
		if(ec)
			shutdown();
	});

	shared_ptr<streambuf> in_buffer = make_shared<streambuf>();
	async_read(
		_socket,
		*in_buffer,
		boost::asio::transfer_exactly(INITIAL_TEXT_LENGTH),
		[this, in_buffer](const error_code& ec, const size_t txsz)
		{
			(void)txsz;
			if(txsz != INITIAL_TEXT_LENGTH || ec)
				shutdown();
			else
			{
				std::istream ins(in_buffer.get());
				deserialiser de(ins);
				string str = de.read_string(INITIAL_TEXT_LENGTH);
				log(MKSTR("client implementation: '" << str << "'"));
				start_read();
			}
		}
	);
}

void connection::terminate()
{
	auto& soc = _socket.lowest_layer();
	error_code ec;

	soc.shutdown(tcp::socket::shutdown_type::shutdown_both, ec);
	if(ec)
		log(MKSTR("socket shutdown: " << ec << " - " << ec.message()));
	soc.close();

	log("closed");
}

void connection::shutdown()
{
	if(_stopped)
		return;

	for(auto it = _channels.begin(); it != _channels.end(); ++it)
		if(it->second->channel_number != 0)
			it->second->shutdown();

	log("shutting down");
	auto self = shared_from_this();
	_server.remove(self); //Self is likly the last reference and is needed for shutdown
	_stopped = true;

	_socket.async_shutdown([this, self](const error_code& ec) {
		log(MKSTR("shutdown: " << ec << " - " << ec.message()));
		self->terminate();
	});
}

void connection::handle_packet(const shared_ptr<packet>& packet)
{
	try
	{
		auto ch_it = _channels.find(packet->channel);
		if(ch_it == _channels.end())
			throw runtime_error(MKSTR("unknown channel:" << packet->channel));
		ch_it->second->handle_packet(packet);
	}
	catch(exception& e)
	{
		cerr << "cannot handle packet: " << e.what() << endl;
		shutdown();
	}
}

void connection::io_error(const string& where, const error_code& ec)
{
	if(!_stopped)
	{
		log(MKSTR(where << ": " << ec << " - " << ec.message()));
		shutdown();
	}
}

shared_ptr<async_stream::token> connection::make_token()
{
	return make_shared<connection_token>(shared_from_this());
}

std::map<const std::string, channel_registry*>* channel_registry::_factories = NULL;

channel_registry::channel_registry(const std::string& type) :
_type(type)
{
	if(_factories && _factories->count(_type))
		throw std::out_of_range(MKSTR("already registered: " << type));

	if(!_factories)
		_factories = new std::map<const std::string, channel_registry*>;

	(*_factories)[type] = this;
}

channel_registry::~channel_registry()
{
	_factories->erase(_type);
	if(_factories->empty())
	{
		delete _factories;
		_factories = NULL;
	}
}

shared_ptr<server_channel> channel_registry::create(const shared_ptr<connection>& stream, const channel_action& action)
{
	if(!_factories || !_factories->count(action.type))
		throw std::out_of_range(MKSTR("invalid channel type: '" << action.type << "'"));

	return (*_factories)[action.type]->create0(stream, action);
}

void channel_registry::init_channels(const config& config)
{
	if(!_factories)
		return;

	for(auto factory : *_factories)
		factory.second->init_channels0(config);
}

server::server(boost::asio::io_service& ios, config& config) :
_connection_counter(0),
_ios(ios),
_acceptor(ios),
_signals(ios),
_context(context::sslv23)
{
	_signals.add(SIGINT);
#if defined(SIGQUIT)
	_signals.add(SIGQUIT);
#endif
	_signals.add(SIGTERM);

	_context.set_options(
		context::default_workarounds
		| context::no_sslv2
		| context::single_dh_use
	);
	_context.load_verify_file(config.ca_crt_file());
	_context.use_certificate_chain_file(config.crt_file());
	_context.use_private_key_file(config.key_file(), context::pem);

	tcp::resolver resolver(ios);
	tcp::endpoint endpoint = *resolver.resolve({config.bind_host(), config.bind_port()});
	_acceptor.open(endpoint.protocol());
	_acceptor.set_option(tcp::acceptor::reuse_address(true));
	_acceptor.bind(endpoint);
	_acceptor.listen();
}

server::~server()
{
}

void server::start()
{
	accept();
	await_signal();

	cerr << "Bound to " << _acceptor.local_endpoint() << endl;
}

void server::await_signal()
{
	_signals.async_wait(
		[this](boost::system::error_code ec, int signo)
		{
			cerr << "received siganl " << signo << ", error code: " << ec << " (" << ec.message() << ")" << endl;
			if(signo == SIGQUIT)
				_ios.stop();
			else
				stop();
		}
	);
}

void server::add(connection::ptr con)
{
	_connections.insert(con);
}

void server::remove(connection::ptr con)
{
	_connections.erase(con);
}

void server::stop()
{
	_acceptor.close();
	while(!_connections.empty())
		(*_connections.begin())->shutdown();
}

void server::accept()
{
	connection::ptr con = std::make_shared<connection, server&, uint32_t>(*this, _connection_counter++);
	con->add_channel0();
	_acceptor.async_accept(
		con->lowest_socket(),
		[this, con](boost::system::error_code ec)
		{
			if(!_acceptor.is_open())
				return;

			if(!ec)
			{
				add(con);
				con->start();
			}

			accept();
		}
	);
}
