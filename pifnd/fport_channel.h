#ifndef _FPORT_CHANNEL_H
#define _FPORT_CHANNEL_H

#include "pqueue.h"
#include "server.h"

#include <boost/asio.hpp>
#include <iostream>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

class fport_channel;

class async_resolve_connect : public std::enable_shared_from_this<async_resolve_connect>
{
private:
	fport_channel& _channel;
	boost::asio::ip::tcp::resolver _resolver;

public:
	static void connect(boost::asio::io_service& ios, fport_channel& channel, const std::string& host, const std::string& port);

private:
	async_resolve_connect(boost::asio::io_service& ios, fport_channel& channel);
	void connect(const std::string& host, const std::string& port);
	void connect(boost::asio::ip::tcp::resolver::iterator);
};

class fport_channel :
	public server_channel,
	private pifn::pconsumer<pifn::packet>,
	private pifn::pauseable_action
{
	friend class async_resolve_connect;

private:
	boost::asio::ip::tcp::socket _socket;
	pifn::pqueue _writeq;
	bool _ready;

	virtual void pqc_next(const std::shared_ptr<pifn::packet>&);
	virtual void pqc_closed();

public:
	fport_channel(const std::shared_ptr<connection>& connection, uint16_t channel_number, const std::string&);
	virtual ~fport_channel();

	void handle_packet(const std::shared_ptr<pifn::packet>& packet);

	virtual void shutdown(const std::string& error = "");
	virtual void write_packet(const std::shared_ptr<pifn::packet>&);

protected:
	virtual void create_notify(const std::string& error = "");
	virtual void action();

private:
	void connect(const std::string& remote);
};

struct target
{
	std::string host, port;

	target();
	target(target&&);
	target(const std::string&);

	bool matches(const target&) const;
	target& operator=(const target&);
};

std::ostream& operator<<(std::ostream&, const target&);

class fport_registrator : public channel_registry
{
private:
	std::map<std::string, target> _aliases;
	std::vector<target> _filters;

public:
	fport_registrator();
	const target& resolve(const target&) const;

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&);
	virtual void init_channels0(const config&);
};

extern fport_registrator fport_registration;

#endif
