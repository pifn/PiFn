#include "fport_channel.h"
#include "protocol.h"
#include "util.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace pifn;
using namespace boost::asio;
using namespace boost::algorithm;
using boost::system::error_code;
using std::cerr;
using std::endl;
using std::invalid_argument;
using std::make_shared;
using std::ostream;
using std::runtime_error;
using std::shared_ptr;
using std::string;

static const string FPORT_NAME = "fport";
static const size_t READ_SIZE = 2048;

void async_resolve_connect::connect(io_service& ios, fport_channel& channel, const string& host, const string& port)
{
	shared_ptr<async_resolve_connect> connector(new async_resolve_connect(ios, channel));
	connector->connect(host, port);
}

async_resolve_connect::async_resolve_connect(io_service& ios, fport_channel& channel) :
_channel(channel),
_resolver(ios)
{
}

void async_resolve_connect::connect(const string& host, const string& port)
{
	ip::tcp::resolver::query query(host, port);
	auto me = shared_from_this();
	_resolver.async_resolve(
		query,
		[me](const error_code& ec, ip::tcp::resolver::iterator iter)
		{
			if(ec)
				me->_channel.create_notify(ec.message());
			else
				me->connect(iter);
		}
	);
}

void async_resolve_connect::connect(ip::tcp::resolver::iterator iter)
{
	auto me = shared_from_this();
	async_connect(
		_channel._socket,
		iter,
		[me](const error_code& ec, ip::tcp::resolver::iterator)
		{
			if(ec)
				me->_channel.create_notify(ec.message());
			else
				me->_channel.create_notify();
		}
	);
}

fport_channel::fport_channel(const shared_ptr<connection>& connection, uint16_t channel_number, const string& remote) :
server_channel(channel_number, connection),
_socket(connection->get_io_service()),
_ready(false)
{
	target r(remote);
	const target& t(fport_registration.resolve(r));
	async_resolve_connect::connect(connection->get_io_service(), *this, t.host, t.port);
}

fport_channel::~fport_channel()
{
}

void fport_channel::pqc_next(const std::shared_ptr<pifn::packet>& packet)
{
	auto me = shared_from_this();
	async_write(
		_socket,
		packet->payload,
		[this, me, packet](const error_code& ec, const size_t)
		{
			if(ec)
				me->shutdown(ec.message());
			_writeq.pop();
		}
	);
}

void fport_channel::pqc_closed()
{
}

void fport_channel::write_packet(const shared_ptr<packet>& packet)
{
	if(_ready)
	{
		server_channel::write_packet(packet);
		trigger();
	}
}

void fport_channel::handle_packet(const std::shared_ptr<pifn::packet>& packet)
{
	_writeq.push(packet);
}

void fport_channel::action()
{
	auto me = shared_from_this();
	auto packet = make_packet();
	_socket.async_receive(
		packet->payload.prepare(READ_SIZE),
		[me, packet](const error_code& ec, const size_t sz)
		{
			if(ec)
				me->shutdown(ec.message());
			else
			{
				packet->payload.commit(sz);
				me->write_packet(packet);
			}
		}
	);
}

void fport_channel::create_notify(const string& error)
{
	if(error.empty())
	{
		_ready = true;
		if(auto con = _con.lock())
		{
			_writeq.add_producer(con->read_pauser());
			con->add_producer(this);
		}
		_writeq.set_consumer(this);
		trigger();
	}
	server_channel::create_notify(error);
}

void fport_channel::shutdown(const string& error)
{
	if(!_ready)
		return;
	_ready = false;

	if(auto con = _con.lock())
	{
		con->remove_producer(this);
		_writeq.remove_producer(con->read_pauser());
	}
	_writeq.set_consumer();

	error_code ec;
	_socket.close(ec);

	server_channel::shutdown(error);
}

target::target()
{
}

target::target(target&& that) :
host(std::move(that.host)),
port(std::move(that.port))
{
}

target::target(const string& spec)
{
	auto sep = spec.find(PATH_SEPARATOR);
	if(sep == string::npos)
		host = spec;
	else if(spec.find(PATH_SEPARATOR, sep + 1) != string::npos)
		throw invalid_argument("too many '" PATH_SEPARATOR "'");
	else
	{
		host = spec.substr(0, sep);
		port = spec.substr(sep + 1);
	}
}

target& target::operator=(const target& that)
{
	host = that.host;
	port = that.port;
	return *this;
}

bool target::matches(const target& that) const
{
	return (host.empty() || host == that.host) && (port.empty() || port == that.port);
}

fport_registrator::fport_registrator() :
channel_registry(FPORT_NAME)
{
}

shared_ptr<server_channel> fport_registrator::create0(const shared_ptr<connection>& connection, const pifn::channel_action& action)
{
	return make_shared<fport_channel>(connection, action.target, action.type_data);
}

void fport_registrator::init_channels0(const config& config)
{
	for(const string& line : config.fport_mapping())
	{
		try
		{
			string::size_type first = string::npos, last = -1;
			size_t count = 0;
			do
			{
				last = line.find(PATH_SEPARATOR, last + 1);
				if(last != string::npos)
					++count;
				if(first == string::npos)
					first = last;
			} while(last != string::npos && count < 3);

			if(count != 2)
				throw invalid_argument(MKSTR("wrong number of '" PATH_SEPARATOR "', expecting 2"));

			//TODO:use a compiler that fully supports c++11
			//_aliases.emplace(line.substr(0, first), line.substr(first + 1));
			_aliases[line.substr(0, first)] = target(line.substr(first + 1));
		}
		catch(const invalid_argument& ex)
		{
			throw invalid_argument(MKSTR("bad mapping: " << ex.what() << ": " << line));
		}
	}

	for(const string& line : config.fport_filters())
	{
		try {
			_filters.emplace_back(line);
		}
		catch(const invalid_argument& ex)
		{
			throw invalid_argument(MKSTR("bad filter: " << ex.what() << ": " << line));
		}
	}
}

const target& fport_registrator::resolve(const target& dest) const
{
	if(dest.port.empty())
	{
		//dest_str is an alias
		auto it = _aliases.find(dest.host);
		if(it != _aliases.end())
			return it->second;
	}
	else
	{
		for(const target& filter : _filters)
			if(filter.matches(dest))
				return dest;
	}

	throw runtime_error(MKSTR("connection denied by pifnd: " << dest));
}

ostream& operator<<(ostream& os, const target& t)
{
	return os << t.host << PATH_SEPARATOR << t.port;
}

fport_registrator fport_registration;
