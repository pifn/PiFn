#include <iostream>
#include <boost/asio.hpp>

#include "shell_channel.h"
#include "configfile.h"
#include "daemon.h"
#include "hardware.h"
#include "server.h"

using namespace std;

int main(int argc, char* argv[])
{
	int rt = EXIT_SUCCESS;

	try
	{
		config cfg;
		if(cfg.init(argc, argv))
			return EXIT_SUCCESS;

		channel_registry::init_channels(cfg);
		daemon_pid dmn(cfg);
		hardware hw(cfg);
		shell_registrator shell_registration(hw);

		boost::asio::io_service ios;

		server server(ios, cfg);

		if(cfg.daemonise())
		{
			ios.notify_fork(boost::asio::io_service::fork_prepare);
			dmn.daemonise(true);
			ios.notify_fork(boost::asio::io_service::fork_child);
		}
		else
			dmn.daemonise(false);

		server.start();

		hw.led(NAVIO_COLOR_BLACK);
		ios.run();
	}
	catch(std::exception& e)
	{
		cerr << "pifnd: " << e.what() << endl;
		rt = EXIT_FAILURE;
	}

	return rt;
}
