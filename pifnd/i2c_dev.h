#ifndef _I2CDEV_H_
#define _I2CDEV_H_

#include <stdint.h>

#include "error.h"

class i2c_dev
{
	public:
		i2c_dev(const char* bus, int addr);
		virtual ~i2c_dev();

		ssize_t chip_write(const void* data, size_t size);
		ssize_t chip_read(void* data, size_t size);

		uint8_t read_reg_byte(const uint8_t reg);
		void write_reg_byte(const uint8_t reg, const uint8_t data);

		void bits_byte(const uint8_t reg, const uint8_t clear, const uint8_t set);

	private:
		int _fd;
};

#endif
