#ifndef _OPTION_DEFS_H
#define _OPTION_DEFS_H

#include "common_option_defs.h"

#define OPT_CONFIG_FILE "configfile"
#define DEC_CONFIG_FILE OPT_CONFIG_FILE ",c"

#define OPT_DETACH "detatch"
#define DEC_DETACH OPT_DETACH ",d"

#define OPT_I2C_BUS "bus"
#define DEC_I2C_BUS OPT_I2C_BUS
#define DEF_I2C_BUS ""

#define OPT_PIDFILE "pidfile"
#define DEC_PIDFILE OPT_PIDFILE
#define DEF_PIDFILE ""

#define OPT_BIND_HOST "bind_host"
#define DEC_BIND_HOST OPT_BIND_HOST
#define DEF_BIND_HOST "127.0.0.1"

#define OPT_BIND_PORT "bind_port"
#define DEC_BIND_PORT OPT_BIND_PORT
#define DEF_BIND_PORT "3497"

#define OPT_KEY_FILE "ssl_key"
#define DEC_KEY_FILE OPT_KEY_FILE
#define DEF_KEY_FILE "ca/private/pifnd.key"

#define OPT_CRT_FILE "ssl_crt"
#define DEC_CRT_FILE OPT_CRT_FILE
#define DEF_CRT_FILE "ca/certs/pifnd.pem"

#define OPT_CA_CRT_FILE "ssl_ca_crt"
#define DEC_CA_CRT_FILE OPT_CA_CRT_FILE
#define DEF_CA_CRT_FILE "ca/cacert.pem"

#define OPT_FPORT_MAP "fport.map"
#define DEC_FPORT_MAP OPT_FPORT_MAP

#define OPT_FPORT_FILTER "fport.filter"
#define DEC_FPORT_FILTER OPT_FPORT_FILTER

#endif
