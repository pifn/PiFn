#include "shell_channel.h"

#include <string>

#define CH_MAX (MT_NUM - 1)

using namespace pifn;
using namespace std;
using boost::asio::io_service;
using boost::asio::buffer_cast;
using boost::asio::buffers_begin;
using boost::asio::buffers_end;

static const string SHELL_NAME = "shell";

mt_timeout::mt_timeout(io_service& ios, hardware& hw, int channel) :
_timer(ios),
_hw(hw),
_channel(channel)
{
}

mt_timeout::~mt_timeout()
{
	cancel();
}

void mt_timeout::timeout(bool break_motor, float timeout)
{
	_break_motor = break_motor;
	_timer.expires_from_now(boost::posix_time::millisec(timeout * 1000.0));
	_timer.async_wait([this](const boost::system::error_code& ec) {
		if(!ec)
			timeout_expiered();
	});
}

void mt_timeout::timeout_expiered()
{
	if(_break_motor)
	{
		_hw.drive(_channel, 0);
		timeout_for_stop();
	}
	else
		_hw.float_stop(_channel);
}

shell_channel::shell_channel(hardware& hw, const shared_ptr<connection>& connection, uint16_t channel_number) :
server_channel(channel_number, connection),
_out(make_packet()),
_sout(&_out->payload),
_hw(hw)
{
	io_service& ios = connection->get_io_service();
	for(int i = 0; i < MT_NUM; ++i)
		_timers[i] = unique_ptr<mt_timeout>(new mt_timeout(ios, hw, i));
	create_notify();
}

void shell_channel::handle_packet(const shared_ptr<packet>& pkt)
{
	typedef boost::asio::buffers_iterator<boost::asio::const_buffers_1, char> iterator;
	const iterator end = buffers_end(pkt->payload.data());

	for(iterator i = buffers_begin(pkt->payload.data()); i != end; ++i)
	{
		char c = *i;
		if(c == '\n')
		{
			parse_command();
			_in.clear();
			_in.str(string());
		}
		else
			_in.put(c);
	}

	if(_out->payload.size())
	{
		shared_ptr<packet> write_packet(_out);
		_out = make_packet();
		_sout.rdbuf(&_out->payload);
		if(auto con = _con.lock())
			con->write_packet(write_packet);
	}
}

void shell_channel::parse_command()
{
	string cmd;
	_in >> cmd;

	if(!cmd.compare("quit"))
		shutdown();
	else if(!cmd.compare("drive"))
		drive();
	else if(!cmd.compare("servo"))
		servo();
	else if(!cmd.compare("pwm"))
		pwm();
	else if(!cmd.compare("led"))
		led();
	else if(!cmd.compare("help"))
		help();
	else if(!cmd.compare("echo"))
		echo();
	else if(cmd.length())
		_sout << "error: Command '" << cmd << "' not recognised, try 'help'" << endl;
}

void shell_channel::echo()
{
	char c;
	bool first = true;
	while(_in.good())
	{
		c = _in.get();
		if(first)
			first = false;
		else if(_in.good())
			_sout << c;
	}
	_sout << endl;
}

int shell_channel::read_int(int min, int max)
{
	int i = min - 1;
	_in >> i;
	if(i < min || i > max)
		i = min - 1;
	return i;
}

float shell_channel::read_float(float min, float max)
{
	float f = read_float(min);
	if(f > max)
		return min - 1;
	return f;
}

float shell_channel::read_float(float min)
{
	float f = min - 1;
	_in >> f;
	if(f < min)
		f = min - 1;
	return f;
}

void shell_channel::drive()
{
	int motor = read_int(0, CH_MAX);
	if(motor < 0)
	{
		_sout << "error: Invalid motor" << endl;
		return;
	}

	int speed = read_int(-7, 7);
	if(speed < -7)
	{
		_sout << "error: Invalid speed" << endl;
		return;
	}

	float timeout = read_float(0, 60);
	if(timeout > 0)
		_timers[motor]->timeout(speed != 0, timeout);
	else if(speed == 0)
		_timers[motor]->timeout_for_stop();
	else
		_timers[motor]->cancel();
	_hw.drive(motor, speed);
}

void shell_channel::servo()
{
	int port = read_int(0, 12);
	if(port < 0)
	{
		_sout << "error: Invalid port" << endl;
		return;
	}

	float position = read_float(-1.0, 1.0);
	if(position < -1)
	{
		_sout << "error: Invalid position" << endl;
		return;
	}

	_hw.servo(port, position);
}

void shell_channel::pwm()
{
	int port = read_int(0, 12);
	if(port < 0)
	{
		_sout << "error: Invalid port" << endl;
		return;
	}

	float duty_cycle = read_float(0.0, 1.0);
	if(duty_cycle < 0)
	{
		_sout << "error: Invalid duty cycle" << endl;
		return;
	}

	_hw.pwm(port, duty_cycle);
}

void shell_channel::led()
{
	float red = read_float(0.0, 1.0);
	float green = read_float(0.0, 1.0);
	float blue = read_float(0.0, 1.0);
	if(red < 0 || green < 0 || blue < 0)
		_sout << "error: Invalid colour" << endl;
	else
		_hw.led(red, green, blue);
}

void shell_channel::help()
{
	_sout << "info: Commands available:" << endl <<
		"info:\tdrive <motor> <speed> [<timeout>]" << endl <<
		"info:\tservo <port> <position>" << endl <<
		"info:\tpwm <port> <duty cycle>" << endl <<
		"info:\tled <red> <green> <blue>" << endl <<
		"info:\tquit" << endl <<
		"info:\thelp" << endl;
}

shell_registrator::shell_registrator(hardware& hardware) :
channel_registry(SHELL_NAME),
_hw(hardware)
{
}

shared_ptr<server_channel> shell_registrator::create0(const shared_ptr<connection>& con, const channel_action& action)
{
	return make_shared<shell_channel>(_hw, con, action.target);
}
