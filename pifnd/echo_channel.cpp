#include "echo_channel.h"
#include "util.h"

using namespace pifn;
using std::cerr;
using std::endl;
using std::shared_ptr;
using std::string;

static const string ECHO_NAME = "echo";
static const string NULL_NAME = "null";

echo_channel::echo_channel(bool echo, uint16_t channel_number, const std::shared_ptr<connection>& connection) :
server_channel(channel_number, connection),
_bytes(0),
_packets(0),
_echo(echo)
{
	create_notify();
	if(_echo)
		if(auto con = _con.lock())
			con->add_producer(con->read_pauser());
}

echo_channel::~echo_channel()
{
	if(_echo)
		if(auto con = _con.lock())
			con->remove_producer(con->read_pauser());
}

void echo_channel::handle_packet(const shared_ptr<packet>& pkt)
{
	++_packets;
	_bytes += pkt->payload.size();

	if(_echo)
		write_packet(pkt);
}

void echo_channel::shutdown(const string& error)
{
	if(auto con = _con.lock())
		con->log(MKSTR((_echo ? "echo" : "null") << " channel recieved " << _bytes
			<< " bytes in " << _packets << " packets"));
	server_channel::shutdown(error);
}

echo_registrator::echo_registrator(const string& name, bool echo) :
channel_registry(name),
_echo(echo)
{
}

shared_ptr<server_channel> echo_registrator::create0(const shared_ptr<connection>& connection, const channel_action& action)
{
	return make_shared<echo_channel>(_echo, action.target, connection);
}

echo_registrator echo_registration(ECHO_NAME, true);
echo_registrator null_registration(NULL_NAME, false);
