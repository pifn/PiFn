#include "server_channel.h"

#include "util.h"

using namespace pifn;
using namespace std;

server_channel_manager::server_channel_manager(uint16_t channel_number, const shared_ptr<connection>& connection) :
server_channel(channel_number, connection)
{
}

void server_channel_manager::create_channel(channel_action& req)
{
	try
	{
		auto con = _con.lock();
		if(con)
			con->add_channel(channel_registry::create(con, req));
	}
	catch(exception& ex)
	{
		fail_channel(req, ex.what());
	}
}

void server_channel_manager::close_channel(channel_action& req)
{
	auto con = _con.lock();
	if(!con)
		return;
	shared_ptr<server_channel> channel = con->get_channel(req.target);
	if(channel)
		channel->shutdown();
	//no fail_channel() call here: closing a non-existant channel here is acceptable
}

void server_channel_manager::fail_channel(channel_action& req, const string& error)
{
	auto con = _con.lock();
	if(!con)
		return;
	req.action |= CR_ACTION_FAIL;
	req.type = "";
	req.type_data =  error;
	con->write_packet(req, 0);
}

void server_channel_manager::handle_packet(const std::shared_ptr<pifn::packet>& packet)
{
	channel_action req;
	streambuf_parse(req, packet->payload);

	switch(req.action)
	{
	case CR_ACTION_CREATE:
		create_channel(req);
		break;
	case CR_ACTION_CLOSE:
		close_channel(req);
		break;
	default:
		fail_channel(req, "unknown action");
		break;
	}
}

manager_registrator::manager_registrator() :
channel_registry("")
{
}

shared_ptr<server_channel> manager_registrator::create0(const std::shared_ptr<connection>& connection, const channel_action& action)
{
	return make_shared<server_channel_manager>(action.target, connection);
}

manager_registrator manager_registration;
