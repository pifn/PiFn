#ifndef _SERVER_CHANNEL
#define _SERVER_CHANNEL

#include "channel.h"
#include "server.h"

#include <memory>

/*
 * server_channel_manager is responsible for handling packets on channel 0.
 * It creates and destorys other channels as requested by incomming packets
 */
class server_channel_manager : public server_channel
{
public:
	server_channel_manager(const server_channel_manager&) = delete;
	server_channel_manager& operator=(const server_channel_manager&) = delete;

	server_channel_manager(uint16_t channel_number, const std::shared_ptr<connection>& connection);

	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);

private:
	void create_channel(pifn::channel_action&);
	void close_channel(pifn::channel_action&);
	void fail_channel(pifn::channel_action&, const std::string&);
};

class manager_registrator : public channel_registry
{
public:
	manager_registrator();

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&);
};

extern manager_registrator manager_registration;

#endif
