#ifndef _SERVER_H
#define _SERVER_H

#include "async_stream.h"
#include "channel.h"
#include "configfile.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/shared_ptr.hpp>
#include <cassert>
#include <map>
#include <memory>
#include <set>

#define IMPLEMENTATION_NAME "pifnd-" PACKAGE_VERSION

class server;

class connection;

/*
 * Instances of server_channel handle packekets for a single channel on a single connection.
 */
class server_channel : public std::enable_shared_from_this<server_channel>, public virtual pifn::packet_handler
{
protected:
	const std::weak_ptr<connection> _con;

public:
	const uint16_t channel_number;

	server_channel(uint16_t, const std::shared_ptr<connection>&);

	virtual void shutdown(const std::string& error = "");
	std::shared_ptr<pifn::packet> make_packet();
	virtual void write_packet(const std::shared_ptr<pifn::packet>& packet);
	virtual void connection_closed() {};

protected:
	/*
	 * All channels must call create_notify to inform the client of the
	 * creation status. If the error string is empty then the client is
	 * informed that the creation was successful. If it is non-empty then then
	 * client is informed that the creation of the channel failed with the
	 * given string, and the channel will later be destroyed.
	 */
	virtual void create_notify(const std::string& error = "");
};

/*
 * A connection to a client
 */
class connection : public std::enable_shared_from_this<connection>, public pifn::async_stream
{
private:
	std::map<uint16_t, std::shared_ptr<server_channel>> _channels;
	server& _server;
	std::string _name;
	bool _stopped;
#ifndef NDEBUG
	bool _started;
#endif

public:
	typedef std::shared_ptr<connection> ptr;

	connection(const connection&) = delete;
	connection& operator=(const connection&) = delete;
	connection(server& manager, uint32_t);
	virtual ~connection();

	void add_channel(std::shared_ptr<server_channel> channel);
	void add_channel0();
	void remove_channel(uint16_t);
	std::shared_ptr<server_channel> get_channel(uint16_t number);

	void start();
	void stop();
	virtual void shutdown();
	server& get_server() { return _server; }

	pifn::ssl_socket::lowest_layer_type& lowest_socket() {return _socket.lowest_layer(); }

	void log(const std::string& message);

protected:
	virtual void io_error(const std::string& where, const boost::system::error_code& ec);
	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);

	class connection_token : public pifn::async_stream::token
	{
	private:
		ptr _ptr;

	public:
		connection_token(const ptr& ptr) : _ptr(ptr) {}
	};
	virtual std::shared_ptr<token> make_token();


private:
	void welcome();
	void terminate();
};

/*
 * channel_registry instance represents a factory for creating server_channel instances.
 * Each registry object will know how to create server_channels for a type, denoted by a string.
 */
class channel_registry
{
private:
	static std::map<const std::string, channel_registry*>* _factories;
	const std::string _type;

public:
	channel_registry(const channel_registry&) = delete;
	channel_registry& operator=(const channel_registry&) = delete;

	channel_registry(const std::string& type);
	virtual ~channel_registry();

	static void init_channels(const config&);
	static std::shared_ptr<server_channel> create(const std::shared_ptr<connection>&, const pifn::channel_action&);

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&) = 0;
	virtual void init_channels0(const config&) {}
};

/*
 * server binds to a socket and spawns connections
 */
class server
{
public:
	server(const server&) = delete;
	server& operator=(const server&) = delete;

	server(boost::asio::io_service& ios, config& config);
	virtual ~server();

	//Start/Stop server
	void start();
	void stop();

	//Start/stop a connection
	void add(connection::ptr con);
	void remove(connection::ptr con);

	boost::asio::ssl::context& ssl_context() {return _context; };
	boost::asio::io_service& io_service() {return _ios; };

private:
	void accept();
	void await_signal();

	uint32_t _connection_counter;
	boost::asio::io_service& _ios;
	boost::asio::ip::tcp::acceptor _acceptor;
	std::set<connection::ptr> _connections;
	boost::asio::signal_set _signals;
	boost::asio::ssl::context _context;
};

#endif
