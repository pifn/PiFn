#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/i2c-dev.h>

#include <system_error>

#include "i2c_dev.h"

using namespace std;

i2c_dev::i2c_dev(const char* bus, int addr)
{
	_fd = open(bus, O_RDWR);

	if(_fd < 0)
		throw system_error(errno, system_category(), "Failed to open device");

	if(ioctl(_fd, I2C_SLAVE, addr) < 0)
		throw system_error(errno, system_category(), "Failed to select device");
}

i2c_dev::~i2c_dev()
{
	if(_fd >= 0)
		close(_fd);
}

ssize_t i2c_dev::chip_write(const void* data, size_t size)
{
	errno = 0;
	ssize_t rt = write(_fd, data, size);
	if(rt < 0 || errno)
		throw system_error(errno, system_category(), "Write failed");

		return rt;
}

ssize_t i2c_dev::chip_read(void* data, size_t size)
{
	errno = 0;
	ssize_t rt = read(_fd, data, size);
	if(rt < 0 || errno)
		throw system_error(errno, system_category(), "Read failed");

		return rt;
}

uint8_t i2c_dev::read_reg_byte(const uint8_t reg)
{
	uint8_t data;
	chip_write(&reg, 1);
	chip_read(&data, 1);
	return data;
}

void i2c_dev::write_reg_byte(const uint8_t reg, const uint8_t data)
{
	uint8_t bytes[2] = {reg, data};
	chip_write(&bytes, 2);
}

void i2c_dev::bits_byte(const uint8_t reg, const uint8_t clear, uint8_t set)
{
	uint8_t data = read_reg_byte(reg);
	data &= ~clear;
	data |= set;
	write_reg_byte(reg, data);
}
