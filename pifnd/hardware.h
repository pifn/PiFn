#ifndef _HARDWARE_H
#define _HARDWARE_H

#include "config.h"

#include "configfile.h"
#include "PCA9685.h"

#define NAVIO_COLOR_BLACK 0, 0, 0
#define NAVIO_COLOR_RED 1, 0, 0
#define NAVIO_COLOR_GREEN 0, 1, 0
#define NAVIO_COLOR_BLUE 0, 0, 1
#define NAVIO_COLOR_YELLOW 1, 1, 0
#define NAVIO_COLOR_ORANGE 1, 0.05, 0
#define NAVIO_COLOR_WHITE 1, 1, 1

#define MT_NUM 8
#define PIFN_ADDR 0x41

class hardware
{
public:
	hardware(const hardware&) = delete;
	hardware& operator=(const hardware&) = delete;
	hardware(config& config);
	virtual ~hardware();

	void drive(int motor, int speed);
	void float_stop(int motor);

	void led(float red, float green, float blue);
	void servo(int port, float position);
	void pwm(int port, float position);

private:
	PCA9685 _pifn;
	PCA9685 _servos;
};

#endif
