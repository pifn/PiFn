#ifndef _GEN_CHANNEL_H
#define _GEN_CHANNEL_H

#include "server.h"

#include <queue>

struct job
{
	uint16_t number;
	uint16_t size;
	bool shutdown;
};

class gen_channel : public server_channel
{
private:
	std::queue<job> _jobs;

	void start_async();

public:
	gen_channel(uint16_t channel_number, const std::shared_ptr<connection>& connection);
	virtual void handle_packet(const std::shared_ptr<pifn::packet>& packet);
	virtual void shutdown(const std::string& error = "");
};

class gen_registrator : public channel_registry
{
public:
	gen_registrator();

protected:
	virtual std::shared_ptr<server_channel> create0(const std::shared_ptr<connection>& connection, const pifn::channel_action&);
};

extern gen_registrator gen_registration;

#endif
