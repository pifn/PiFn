all: newcerts private certs index.txt index.txt.attr serial certs/pifnd.pem

KEYSIZE=4096

private:
	mkdir $@
	chmod 700 $@

newcerts:
	mkdir $@

certs:
	mkdir $@

%.key:
	openssl genrsa -out $@ $(KEYSIZE) && chmod 400 $@

serial:
	echo 1000 > $@

index.txt:
	touch $@

index.txt.attr:
	echo "unique_subject = no" > $@

cacert.pem: certs private private/ca.key
	test -e $@ || openssl req -config ../openssl.conf -new -sha256 -x509 -days 3650 -key private/ca.key -out $@ -extensions v3_ca -subj "/C=ZZ/ST=Unspecified/L=Unspecified/O=Pi Functions/OU=Root CA/CN=`hostname`" && touch $@

certs/pifnd.pem : certs private private/pifnd.key private/ca.key cacert.pem
	test -e $@ || openssl req -config ../openssl.conf -new -sha256 -x509 -days 3650 -key private/pifnd.key -out $@ -extensions usr_cert -subj "/C=ZZ/ST=Unspecified/L=Unspecified/O=Pi Functions/OU=pifnd/CN=`hostname`" && touch $@
