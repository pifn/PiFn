#include "PCA9685.h"

#define PRESCALE(x) (roundf(_clk_frq / 4096.f / x) - 1)
#define FRQ(x) (_clk_frq / 4096.f / (x + 1))
#define M1EC(x) (_int_clk ? ((x) | (1 << PCA9685_MODE1_EXTCLK_BIT)) : (x))

PCA9685::PCA9685(const char *i2cDev, uint8_t address) :
_dev(*i2cDev ? new i2c_dev(i2cDev, address) : NULL),
_out_frq(0),
_clk_frq(0),
_int_clk(false)
{
}

void PCA9685::init_prescale(bool int_clk, float out_frq, float clk_frq)
{
	_int_clk = int_clk;
	_clk_frq = clk_frq;
	uint8_t prescale = PRESCALE(out_frq);

	if(_dev)
	{
		sleep();
		usleep(10000);

		_dev->write_reg_byte(PCA9685_RA_PRE_SCALE, prescale);
		prescale = _dev->read_reg_byte(PCA9685_RA_PRE_SCALE);

		_dev->write_reg_byte(PCA9685_RA_MODE1, M1EC(1 << PCA9685_MODE1_AI_BIT));
	}
	_out_frq = FRQ(prescale);
}

float PCA9685::initialize(float out_frq)
{
	init_prescale(false, out_frq, PCA9685_INT_CLK_FRQ);
	return _out_frq;
}

float PCA9685::initialize(float out_frq, float clk_frq)
{
	init_prescale(true, out_frq, clk_frq);
	return _out_frq;
}

void PCA9685::sleep()
{
	if(_dev)
		_dev->bits_byte(PCA9685_RA_MODE1, 0, 1 << PCA9685_MODE1_SLEEP_BIT);
}

void PCA9685::restart()
{
	if(_dev)
	{
		uint8_t sleep_mask = 1 << PCA9685_MODE1_SLEEP_BIT;
		_dev->bits_byte(PCA9685_RA_MODE1, sleep_mask, 0);
		_dev->bits_byte(PCA9685_RA_MODE1, 0, sleep_mask);
	}
}

float PCA9685::getFrequency()
{
	return _out_frq;
}

void PCA9685::setPWM(uint8_t channel, uint16_t offset, uint16_t length)
{
	if(!_dev)
		return;

	uint8_t data[5];
	memset(data, 0, sizeof(data));

	if(channel > 16)
		return;
	if(channel == 16)
		data[0] = PCA9685_RA_ALL_LED_ON_L;
	else
		data[0] = PCA9685_RA_LED0_ON_L + 4 * channel;

	if(length == 0)
	{
		data[4] = 0x10;
	}
	else if(length >= 4096)
	{
		data[2] = 0x10;
	}
	else
	{
		data[1] = offset & 0xFF;
		data[2] = offset >> 8;
		data[3] = length & 0xFF;
		data[4] = length >> 8;
	}
	_dev->chip_write(&data, 5);
}

void PCA9685::setPWM(uint8_t channel, uint16_t length)
{
	setPWM(channel, 0, length);
}

void PCA9685::setPWMmS(uint8_t channel, float length_mS)
{
	setPWM(channel, round((length_mS * 4096.f) / (1000.f / _out_frq) - 1));
}

void PCA9685::setPWMuS(uint8_t channel, float length_uS)
{
	setPWM(channel, round((length_uS * 4096.f) / (1000000.f / _out_frq) - 1));
}
