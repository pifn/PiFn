#include "gen_channel.h"

#include <algorithm>
#include <cstring>

using namespace pifn;
using boost::asio::streambuf;
using boost::asio::buffer_cast;
using std::make_shared;
using std::min;
using std::shared_ptr;
using std::string;

static const string GEN_NAME = "gen";
static const char DATA[] = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', ' '};

gen_channel::gen_channel(uint16_t channel_number, const shared_ptr<connection>& connection) :
server_channel(channel_number, connection)
{
	create_notify();
}

void gen_channel::start_async()
{
	while(!_jobs.empty() && _jobs.front().number == 0)
	{
		if(_jobs.front().shutdown)
			shutdown();
		else
			_jobs.pop();
	}

	if(!_jobs.empty())
	{
		auto packet = make_packet();
		const uint16_t size = _jobs.front().size;

		char* data = buffer_cast<char*>(packet->payload.prepare(size));
		uint16_t i = 0;
		do
		{
			const size_t cp = min(sizeof(DATA), (size_t)(size - i));
			memcpy(&data[i], DATA, cp);
			i += cp;
		}
		while(i < size);
		packet->payload.commit(size);

		auto me = shared_from_this();
		if(auto con = _con.lock())
			con->write_packet(
				packet,
				[this, packet, me]()
				{
					start_async();
				}
			);
		--(_jobs.front().number);
	}
}

void gen_channel::handle_packet(const shared_ptr<packet>& pkt)
{
	bool start_sending = _jobs.empty();
	const uint16_t* data = buffer_cast<const uint16_t*>(pkt->payload.data());
	const size_t size = pkt->payload.size() & ~0x03;
	for(size_t i = 0; i < size; i += 4)
	{
		job j;
		j.number = ntohs(data[i]);
		j.size = ntohs(data[i + 1]);
		j.shutdown = j.number == 0 && j.size == 0;
		_jobs.push(j);
	}
	if(start_sending)
		start_async();
}

void gen_channel::shutdown(const std::string& error)
{
	while(!_jobs.empty())
		_jobs.pop();
	server_channel::shutdown(error);
}

gen_registrator::gen_registrator() :
channel_registry(GEN_NAME)
{
}

shared_ptr<server_channel> gen_registrator::create0(const shared_ptr<connection>& connection, const pifn::channel_action& action)
{
	return make_shared<gen_channel>(action.target, connection);
}

gen_registrator gen_registration;
